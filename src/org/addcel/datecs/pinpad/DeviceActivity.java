package org.addcel.datecs.pinpad;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.addcel.datecs.pinpad.PinpadManager.OnConnectListener;
import org.addcel.mobilecardpos.R;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.os.Bundle;
import android.os.Handler;
import android.util.Pair;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.TwoLineListItem;

@SuppressWarnings("deprecation")
public class DeviceActivity extends Activity {
	private PinpadManager mPinpadManager;
	private ListView mListView;
	private List<Pair<String, String>> mList;
	 
	private final Handler mHandler = new Handler();
	
	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_device);
        setResult(RESULT_CANCELED);
        
        mPinpadManager = PinpadManager.getInstance(this);
        mPinpadManager.setOnConnectListener(new OnConnectListener() {			
			@Override
			public void OnConnect() {
				mHandler.post(new Runnable() {
					public void run() {								
						setResult(RESULT_OK);
						finish();
					}
				});						
			}
		});
        mPinpadManager.enableServerSocket();
        
        mList = new ArrayList<Pair<String,String>>();
        mListView = (ListView)findViewById(R.id.listView);        
        mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parentView, View view, int position, long id) {
				final Pair<String, String> data = mList.get(position);
				final String bthAddress = data.second;
				
				final ProgressDialog dialog = new ProgressDialog(DeviceActivity.this);
				dialog.setMessage("Espere porfavor");
				dialog.setOnKeyListener(new OnKeyListener() {					
					@Override
					public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
						return true;
					}
				});
				dialog.show();
				
				mPinpadManager.tryConnect(bthAddress, new Runnable() {					
					@Override
					public void run() {
						dialog.dismiss();
						
						mHandler.post(new Runnable() { 
							public void run() {								
								if (mPinpadManager.isConnected()) {
									Toast.makeText(getApplicationContext(), "Pinpad Conectado", Toast.LENGTH_SHORT).show();
								} else {
									IOException e = mPinpadManager.getException();
									String message = "Fallo al conectar con Pinpad" +  ": " ;
									if (e != null) {
										message += e.getMessage();
									}
									Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
								}
							}
						});		
					}
				});
			}			
		});
        
        findViewById(R.id.cancel).setOnClickListener(new OnClickListener() {		
			@Override
			public void onClick(View v) {
				finish();
			}
		});
        
        loadDevices();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	mPinpadManager.disableServerSocket();
    }
    
    private void loadDevices() {
    	final BluetoothAdapter bthAdapter = BluetoothAdapter.getDefaultAdapter();    	
    	if (bthAdapter == null) { 
    		return;
    	}
    	    	    	
    	final ArrayAdapter<Pair<String, String>> arrayAdapter = new ArrayAdapter<Pair<String,String>>(this, android.R.layout.simple_list_item_2, mList) {
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				TwoLineListItem row;
                if(convertView == null){
                    LayoutInflater inflater = (LayoutInflater)getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    row = (TwoLineListItem)inflater.inflate(android.R.layout.simple_list_item_2, null);
                } else{
                    row = (TwoLineListItem)convertView;
                }
                
                final Pair<String, String> data = mList.get(position);
                row.getText1().setText(data.first);
                row.getText2().setText(data.second);
	            return row;	            
			}    		            
        };
        mListView.setAdapter(arrayAdapter);
    	
    	final Set<BluetoothDevice> paired = bthAdapter.getBondedDevices();    	
    	for (BluetoothDevice device: paired) {
    		final Pair<String, String> data = new Pair<String, String>(device.getName(), device.getAddress());
    		mList.add(data);    		
    	}
    	
    	arrayAdapter.notifyDataSetChanged();
    }       
}
