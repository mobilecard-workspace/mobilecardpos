package org.addcel.datecs.pinpad.tlv;

/**
 * Tag class type
 */
public enum TagClass {
    UNIVERSAL, 
    APPLICATION, 
    CONTEXT_SPECIFIC, 
    PRIVATE
}