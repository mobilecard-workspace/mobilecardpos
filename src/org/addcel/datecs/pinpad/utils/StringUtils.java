package org.addcel.datecs.pinpad.utils;

public class StringUtils {
	
	public static final String padLeft(String str, int size, char padChar) {
		final StringBuffer padded = new StringBuffer(str);
		
		while (padded.length() < size)  {
		    padded.insert(0, padChar);
		}
		
		return padded.toString();		
	}
	
	public static final String padRight(String str, int size, char padChar) {
	    final StringBuffer padded = new StringBuffer(str);
		
		while (padded.length() < size)  {
		    padded.append(padChar);
		}
		
		return padded.toString();		
	}
}
