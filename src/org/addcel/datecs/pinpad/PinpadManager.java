package org.addcel.datecs.pinpad;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.datecs.pinpad.Pinpad;
import com.datecs.pinpad.Pinpad.PinpadListener;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;

public class PinpadManager {
	private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	
	private static PinpadManager sInstance = null;
	
	public interface OnConnectListener {
		public void OnConnect();
	};
	
	public interface OnDisconnectListener {
		public void OnDisconnect();
	};
		
	private BluetoothAdapter mBthAdapter;
	private BluetoothSocket mBthSocket;
	private BluetoothDevice mBthDevice;
	private BluetoothServerSocket mBthServerSocket;
	private OnConnectListener mOnConnectListener;
	private OnDisconnectListener mOnDisconnectListener;
	private Pinpad mPinpad;
	private boolean mConnected;
	private IOException mException;
		
	private PinpadManager(Context context) {
		mBthAdapter = BluetoothAdapter.getDefaultAdapter();
		mConnected = false;
		
		Pinpad.setDebug(true);
	}
	
	public static PinpadManager getInstance(Context context) {		
		if (sInstance == null) {
			sInstance = new PinpadManager(context); 
		}
		
		return sInstance;
	}
	
	public void enableServerSocket() {
		try {
			mBthServerSocket = mBthAdapter.listenUsingRfcommWithServiceRecord("SPP", SPP_UUID);			
		} catch (IOException e) {			
			e.printStackTrace();
			return;
		}
		
		final Thread t = new Thread(new Runnable() {			
			@Override
			public void run() {
				while (true) {
					BluetoothSocket socket = null;
					
					try {
						socket = mBthServerSocket.accept();
						createPinpad(socket);
						mBthDevice = socket.getRemoteDevice();
						setConnected(true);
					} catch (IOException e) {
						e.printStackTrace();
						break;
					}
				}				
			}
		});
		t.start();
	}
	
	public void disableServerSocket() {
		if (mBthServerSocket != null) {
			try {
				mBthServerSocket.close();
				mBthServerSocket = null;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public IOException getException() {
		return mException;
	}
	
	public void tryConnect(final String address, final Runnable finish) {
		final Thread t = new Thread(new Runnable() {			
			@Override
			public void run() {
				boolean connected = false;
				
				mBthAdapter.cancelDiscovery(); 				
				
				try {
					final BluetoothDevice device = mBthAdapter.getRemoteDevice(address);
					//final BluetoothSocket socket = device.createRfcommSocketToServiceRecord(SPP_UUID);
					final BluetoothSocket socket = device.createInsecureRfcommSocketToServiceRecord(SPP_UUID);
					socket.connect();					
					createPinpad(socket);
					mBthDevice = device;
					connected = true;					
				} catch (IOException e) {
					e.printStackTrace();
					mException = e;
				} finally {
					if (connected) {
						setConnected(true);
					}
					
					finish.run();
				}
			}
		});
		t.start();
	}
	
	public synchronized void release() {
		if (mPinpad != null) {
			mPinpad.setPinpadListener(null);
			mPinpad.release();
		}
		
		if (mBthSocket != null) {
			try {
				mBthSocket.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		disableServerSocket();	
		
		mConnected = false;
	}
	
	private synchronized void createPinpad(BluetoothSocket socket) throws IOException {
		final InputStream in = socket.getInputStream();
		final OutputStream out = socket.getOutputStream();
		final Pinpad pinpad = new Pinpad(in, out);
		pinpad.sysBeep(4096, 256, 50);
				
		if (mConnected) {
			pinpad.release();
			socket.close();
			throw new IOException("The device is already connected");
		} else {		
			mPinpad = pinpad;
			mPinpad.setPinpadListener(new PinpadListener() {				
				@Override
				public void onPinpadRelease() {
					setConnected(false);
				}
			});			
		}
	}
	
	private synchronized void setConnected(boolean state) {
		if (state) {
			mConnected = true;
			if (mOnConnectListener != null) {
				mOnConnectListener.OnConnect();								
			}
						
		} else {
			mConnected = false;
			if (mOnDisconnectListener != null) { 
				mOnDisconnectListener.OnDisconnect();											
			}			
		}
	}
	
	public void setOnConnectListener(OnConnectListener listener) {
		mOnConnectListener = listener;		
		
	}
	
	public void setOnDisconnectListener(OnDisconnectListener listener) {
		mOnDisconnectListener = listener;
	}

	public Pinpad getPinpad() {
		return mPinpad;
	}	
	
	public String getBluetoothName() {
		return mBthDevice.getName();
	}
	
	public String getBluetoothAddress() {
		return mBthDevice.getAddress();
	}
	
	public boolean isConnected() {
		return mConnected;
	}
}
