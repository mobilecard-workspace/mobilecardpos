package org.addcel.datecs.pinpad;

import java.io.IOException;

import org.addcel.datecs.pinpad.utils.StringUtils;

import android.os.SystemClock;

import com.datecs.pinpad.Pinpad;
import com.datecs.pinpad.PinpadException;

public class PinpadHelper {
	// Show default application screen
	public static final void showDefaultScreen(Pinpad pinpad) throws IOException, PinpadException {
		pinpad.sysStatusLine(true);
		pinpad.uiStopAnimation(-1);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);   
    	pinpad.uiKeyboardControl(false);
	}
		
	public static final void abortTransaction(Pinpad pinpad) throws PinpadException, IOException {
		pinpad.sysStatusLine(false);
		pinpad.uiStopAnimation(-1);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);    	
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\n  TRANSACTION\n    ABORTED");
		SystemClock.sleep(1000);
		showDefaultScreen(pinpad);
    }
	
	public static final void cancelTransaction(Pinpad pinpad) throws PinpadException, IOException {
		pinpad.sysStatusLine(false);
		pinpad.uiStopAnimation(-1);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);    	
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\n  TRANSACTION\n   CANCELLED");
		SystemClock.sleep(1000);
		showDefaultScreen(pinpad);
    }
    
	public static final void successTransaction(Pinpad pinpad) throws PinpadException, IOException  {
		pinpad.sysStatusLine(false);
		pinpad.uiStopAnimation(-1);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);    	
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\n  TRANSACTION\n   SUCCESSFUL");
		SystemClock.sleep(1000);
		showDefaultScreen(pinpad);
    }
	
	public static final void deniedTransaction(Pinpad pinpad) throws PinpadException, IOException  {
		pinpad.sysStatusLine(false);
		pinpad.uiStopAnimation(-1);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);    	
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\n  TRANSACTION\n    DENIED");
		SystemClock.sleep(1000);
		showDefaultScreen(pinpad);
    }
    
	public static final void busyTransaction(Pinpad pinpad) throws PinpadException, IOException  {
    	pinpad.uiStopAnimation(-1);
    	pinpad.uiStartAnimation(50, 30, Pinpad.ANIM_BUSY, true);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);
    	pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\n   PLEASE WAIT");    	
    }
    
	// Check if card is available into reader
	public static final boolean isCardAvailable(Pinpad pinpad) throws PinpadException, IOException {
    	try {
			pinpad.scCheckCard(Pinpad.SC_SLOT_MAIN);
			return true; 
		} catch (PinpadException e) {
			if (e.getErrorCode() == Pinpad.ERROR_NO_DATA) {
				return false;
			}
			throw e;
		}
    }
	
	// Check if card is available into reader
	public static final int readKey(Pinpad pinpad) throws PinpadException, IOException {
		pinpad.uiKeyboardControl(true);
		
		try {    		
    		int key = pinpad.uiReadKey();
    		System.out.println("Key: " + key);
    		return key;
		} catch (PinpadException e) {
			if (e.getErrorCode() == Pinpad.ERROR_NO_DATA) {
				return 0;
			}
			throw e;
		}
    }
	
	public static final boolean insertCard(Pinpad pinpad, int timeout) throws PinpadException, IOException, InterruptedException {
		pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
		pinpad.uiStartAnimation(60, 0, Pinpad.ANIM_INSERT_CARD, true);
    	pinpad.uiFillScreen(Pinpad.COLOR_WHITE);
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\nPLEASE\nINSERT\nCARD");
		
		long end = System.currentTimeMillis() + timeout;
    	while (true) {
    		if (isCardAvailable(pinpad)) {
    			break;
    		}
    		
    		int key = readKey(pinpad); 
    		
    		if (key == 1) {
    			return false;
    		}
    		
    		if (key == 'C') {
    			throw new InterruptedException("Transaction cancelled");
    		}
    		
    		if (System.currentTimeMillis() > end) {
    			throw new InterruptedException("Timeout expired");
    		}
    		
    		SystemClock.sleep(100);    			
    	} 
    	
    	return true;
    }
    
	public static final byte[] removeCard(Pinpad pinpad, boolean readCard) throws PinpadException, IOException {
		byte[] result = null;
		boolean beep = true;
		
	    pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
		pinpad.uiStartAnimation(60, 0, Pinpad.ANIM_REMOVE_CARD, true);
		pinpad.uiFillScreen(Pinpad.COLOR_WHITE);
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + "\nPLEASE\nREMOVE\nCARD");		
		
		if (readCard) {
		    pinpad.msStart();
		}
		
		while (true) {
		    if (readCard && result == null) {
    		    try { 
    		        pinpad.msPull();
    		        result = pinpad.msGetCardData3DESCBC(3, 0x11223344);
    		    } catch (PinpadException e) {
    		        if (e.getErrorCode() != Pinpad.ERROR_NO_DATA) {
    		            throw e;
    		        }
    		    }
		    }
		        
    		if (!isCardAvailable(pinpad)) {
    			break;
    		}    
    		
    		if (beep) {
    		    pinpad.sysBeep(2000, 300, 50);
    		    beep = false;
    		} else {
    		    SystemClock.sleep(300);
    		    beep = true;
    		}    		
    	}    
		
		if (readCard) {
		    pinpad.msStart();
		}
		
		return result;
    }
	
	public static final boolean confirm(Pinpad pinpad, String message) throws PinpadException, IOException {
		pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
		pinpad.uiFillScreen(Pinpad.COLOR_WHITE);
		pinpad.uiDrawString(new String(new byte[] { 0x01 }) + message);
				
    	while (true) {
    		int key = readKey(pinpad);
    		
    		if (key == 'A') {
    			return true;
    		}
    		
    		if (key == 'C') {
    			return false;
    		}
    		
    		SystemClock.sleep(100);    			
    	}    	
	}
	
	public static final int menu(Pinpad pinpad, String[] items) throws PinpadException, IOException, InterruptedException {
		int lines = 3;
		int start = 0;
		int index = 0;
		boolean redraw = true;
		
		pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
    	
    	while (true) {
    		if (redraw) {
    			String s = new String(new byte[] {0x01, 0x0B}) + "   SELECT APP   " + new String(new byte[] {0x0C});    			
    			
    			for (int i = start; i < (start + lines) && i < items.length; i++) {
    				if (i == index) {
    					s += StringUtils.padRight(">" + items[i], 16, ' ').substring(0, 16); 
    				} else {
    					s += StringUtils.padRight(" " + items[i], 16, ' ').substring(0, 16);
    				}    				
    			}
    			pinpad.uiFillScreen(Pinpad.COLOR_WHITE);    
				pinpad.uiDrawString(s);
    			redraw = false;
    		}
    		
    		int key = readKey(pinpad);
    		
    		switch (key) {
    			case 'A': return index;
    			case 'C': throw new InterruptedException("Selection canceled");
    			case 'a': {
	    			if (index > 0) index--;
	    			if (start > index) start--;
	    			redraw = true;
	    			break;
	    		}
    			case 'b': {
    				if (index < (items.length - 1)) index++;
    				if ((index - start) >= lines) start++;    				
    				redraw = true;
    				break;
    			}
    			default: {
    				SystemClock.sleep(100);
    			}
    		}    		    		
    	}    			
	}	
	
	public static final String enterPAN(Pinpad pinpad, int timeout) throws PinpadException, IOException, InterruptedException {
		pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
    	
		//byte[] msg = new byte[] {0x1b, 0x05, 0x1b, 0x2f};	
    	byte[] msg = new byte[] {0x1b, 0x05, 0x20, 0x1b, 0x15, 0x3a};
		pinpad.uiEnableNumericMode(0, 1, Pinpad.LANG_ENGLISH, msg);
    	   
		StringBuffer res = new StringBuffer();
		try {			
			long end = System.currentTimeMillis() + timeout;		
	    	while (true) {
	    		int key = readKey(pinpad); 
	    		
	    		if (key >= '0' && key <= '9') {
	    			if (res.length() < 16) {
	    				pinpad.uiDrawString("" + (char)key);
	    				res.append((char)key);
	    			} else {
	    				pinpad.sysBeep(2000, 500, 50);
	    			}
	    		} else if (key == 'c') {
	    			int length = res.length();
	    			if (length > 0) {
	    				pinpad.uiDrawString("" + (char)0x08);
	    				res.delete(length -1, length);	    				
	    			}	    			
	    		} else if (key == 'A') {
	    			if (res.length() < 13) {
	    				pinpad.sysBeep(2000, 500, 50);
	    			} else {
	    				break;
	    			}
	    		} else if (key == 'C') {
	    			throw new InterruptedException("Transaction cancelled");
	    		} else {	    		
		    		if (System.currentTimeMillis() > end) {
		    			throw new InterruptedException("Timeout expired");
		    		}
	    		}
	    		
	    		SystemClock.sleep(50);    			
	    	}
		} finally {
			pinpad.uiDisableNumericMode();
		}
    	
    	return res.toString();
    }
	
	public static final String enterExpiryDate(Pinpad pinpad, int timeout) throws PinpadException, IOException, InterruptedException {
		pinpad.sysStatusLine(false);
    	pinpad.uiStopAnimation(-1);
    			
		byte[] msg = new byte[] {0x1b, 0x05, 0x20, 0x1b, 0x18, 0x3a};
    	pinpad.uiEnableNumericMode(0, 1, Pinpad.LANG_ENGLISH, msg);
    	    	
		StringBuffer res = new StringBuffer();
		try {
			long end = System.currentTimeMillis() + timeout;
	    	while (true) {
	    		int key = readKey(pinpad); 
	    		
	    		if (key >= '0' && key <= '9') {
	    			int length = res.length();
	    			
	    			if (length == 2) {
	    				pinpad.uiDrawString("/");
	    				res.append('/');
	    			} 
	    			
	    			if (length < 5) {
	    				pinpad.uiDrawString("" + (char)key);
	    				res.append((char)key);
	    			} else {
	    				pinpad.sysBeep(2000, 500, 50);
	    			}
	    		} else if (key == 'c') {
	    			int length = res.length();
	    			if (length == 4) {
	    				pinpad.uiDrawString("" + (char)0x08 + (char)0x08);
	    				res.delete(length - 2, length);
	    			} else if (length > 0) {
	    				pinpad.uiDrawString("" + (char)0x08);
	    				res.delete(length -1, length);
	    			}
	    		} else if (key == 'A') {
	    			if (res.length() < 5) {
	    				pinpad.sysBeep(2000, 500, 50);
	    			} else {
	    				break;
	    			}
	    		} else if (key == 'C') {
	    			throw new InterruptedException("Transaction cancelled");
	    		} else {		    		
		    		if (System.currentTimeMillis() > end) {
		    			throw new InterruptedException("Timeout expired");
		    		}
	    		
		    		SystemClock.sleep(50);
	    		}
	    	}
		} finally {
			pinpad.uiDisableNumericMode();
		}
    	
    	return res.toString();
    }
}
