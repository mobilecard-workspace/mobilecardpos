package org.addcel.datecs.activities;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.security.MessageDigest;
import java.util.Arrays;

import org.addcel.datecs.util.CryptoUtil;
import org.addcel.datecs.util.HexUtil;
import org.addcel.mobilecardpos.views.PrimaProActivity;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReader.Battery;
import com.datecs.audioreader.AudioReader.CardInfo;
import com.datecs.audioreader.AudioReader.DeviceKey;
import com.datecs.audioreader.AudioReader.DeviceKeysInfo;
import com.datecs.audioreader.AudioReader.FinancialCard;
import com.datecs.audioreader.AudioReader.FirmwareInformation;
import com.datecs.audioreader.AudioReader.Identification;
import com.datecs.audioreader.AudioReader.OnUpdateFirmwareListener;
import com.datecs.audioreader.AudioReaderException;
import com.datecs.audioreader.rfid.ContactlessCard;
import com.datecs.audioreader.rfid.FeliCaCard;
import com.datecs.audioreader.rfid.ISO14443Card;
import com.datecs.audioreader.rfid.ISO15693Card;
import com.datecs.audioreader.rfid.RFIDException;
import com.datecs.audioreader.rfid.STSRICard;

public class AudioReaderHelper {

	/**
	 * Default RSA private key modulus.
	 */
	public static final byte[] PRIV_KEY_MOD_BYTES = { (byte) 0x77, (byte) 0x6C,
			(byte) 0xD1, (byte) 0xEF, (byte) 0x62, (byte) 0xE9, (byte) 0x8D,
			(byte) 0x8F, (byte) 0x19, (byte) 0xB3, (byte) 0x4F, (byte) 0xDA,
			(byte) 0xD2, (byte) 0x41, (byte) 0x1C, (byte) 0x7A, (byte) 0xC7,
			(byte) 0xE8, (byte) 0xD9, (byte) 0x5D, (byte) 0x10, (byte) 0xD4,
			(byte) 0xF4, (byte) 0xA5, (byte) 0x68, (byte) 0x26, (byte) 0xAA,
			(byte) 0x2C, (byte) 0xCE, (byte) 0x8E, (byte) 0xA3, (byte) 0x3C,
			(byte) 0x05, (byte) 0xEA, (byte) 0x1B, (byte) 0x81, (byte) 0x6A,
			(byte) 0x39, (byte) 0xDE, (byte) 0x34, (byte) 0x7B, (byte) 0x23,
			(byte) 0xC5, (byte) 0xE6, (byte) 0x25, (byte) 0x50, (byte) 0x73,
			(byte) 0x55, (byte) 0xD8, (byte) 0x3F, (byte) 0x3F, (byte) 0x33,
			(byte) 0x2E, (byte) 0x5B, (byte) 0x28, (byte) 0xB9, (byte) 0xFE,
			(byte) 0x4C, (byte) 0x40, (byte) 0xAA, (byte) 0xD2, (byte) 0x40,
			(byte) 0x1A, (byte) 0xE4, (byte) 0x37, (byte) 0x85, (byte) 0x33,
			(byte) 0x87, (byte) 0x32, (byte) 0x46, (byte) 0xAE, (byte) 0xCE,
			(byte) 0x54, (byte) 0x03, (byte) 0xD2, (byte) 0xAD, (byte) 0x8A,
			(byte) 0xE0, (byte) 0xAF, (byte) 0x27, (byte) 0xC6, (byte) 0x03,
			(byte) 0x7C, (byte) 0xCF, (byte) 0x78, (byte) 0x96, (byte) 0x17,
			(byte) 0xF5, (byte) 0x5A, (byte) 0x2D, (byte) 0x38, (byte) 0x94,
			(byte) 0x28, (byte) 0x2B, (byte) 0x6F, (byte) 0xD9, (byte) 0xEC,
			(byte) 0xA0, (byte) 0x7C, (byte) 0x5F, (byte) 0xDE, (byte) 0x20,
			(byte) 0xE8, (byte) 0x2F, (byte) 0xA6, (byte) 0x51, (byte) 0x07,
			(byte) 0xCD, (byte) 0xD5, (byte) 0xB0, (byte) 0xC8, (byte) 0xB0,
			(byte) 0x44, (byte) 0xB0, (byte) 0x4A, (byte) 0xE2, (byte) 0x5B,
			(byte) 0xD7, (byte) 0xC4, (byte) 0x99, (byte) 0x22, (byte) 0x98,
			(byte) 0xAC, (byte) 0x95, (byte) 0x75, (byte) 0x99, (byte) 0x5D,
			(byte) 0xEB, (byte) 0xBB, (byte) 0x97, (byte) 0x22, (byte) 0x82,
			(byte) 0xC0, (byte) 0xF4, (byte) 0x6A, (byte) 0x4E, (byte) 0x0E,
			(byte) 0x74, (byte) 0xE3, (byte) 0xA8, (byte) 0x11, (byte) 0x17,
			(byte) 0xBA, (byte) 0x0F, (byte) 0xD1, (byte) 0x47, (byte) 0x7E,
			(byte) 0x38, (byte) 0x96, (byte) 0xA0, (byte) 0xDA, (byte) 0x5F,
			(byte) 0x99, (byte) 0x1B, (byte) 0x6B, (byte) 0x68, (byte) 0x76,
			(byte) 0x46, (byte) 0x9C, (byte) 0xED, (byte) 0x6A, (byte) 0x5F,
			(byte) 0xE3, (byte) 0x3A, (byte) 0xA0, (byte) 0x03, (byte) 0x5D,
			(byte) 0xBC, (byte) 0x27, (byte) 0x2B, (byte) 0x45, (byte) 0xC1,
			(byte) 0x29, (byte) 0xBA, (byte) 0x6D, (byte) 0x6B, (byte) 0xF0,
			(byte) 0xBF, (byte) 0x8A, (byte) 0x93, (byte) 0xBB, (byte) 0x9C,
			(byte) 0x34, (byte) 0xB6, (byte) 0xB1, (byte) 0xC9, (byte) 0x33,
			(byte) 0xC8, (byte) 0x3B, (byte) 0x53, (byte) 0xE2, (byte) 0xE7,
			(byte) 0x40, (byte) 0xF7, (byte) 0x30, (byte) 0x74, (byte) 0x98,
			(byte) 0xF1, (byte) 0x7D, (byte) 0xB5, (byte) 0x60, (byte) 0x7C,
			(byte) 0x55, (byte) 0x28, (byte) 0x73, (byte) 0x19, (byte) 0x5C,
			(byte) 0x74, (byte) 0x22, (byte) 0xB7, (byte) 0xB8, (byte) 0x65,
			(byte) 0xFC, (byte) 0xA1, (byte) 0xBA, (byte) 0x3A, (byte) 0xC3,
			(byte) 0x4D, (byte) 0x70, (byte) 0x75, (byte) 0xFE, (byte) 0x95,
			(byte) 0x6A, (byte) 0x96, (byte) 0x0F, (byte) 0xC2, (byte) 0x75,
			(byte) 0x86, (byte) 0xB1, (byte) 0x26, (byte) 0x00, (byte) 0x07,
			(byte) 0x20, (byte) 0x02, (byte) 0x35, (byte) 0x50, (byte) 0x23,
			(byte) 0xA0, (byte) 0x94, (byte) 0x47, (byte) 0xC7, (byte) 0x1D,
			(byte) 0x4F, (byte) 0x72, (byte) 0x77, (byte) 0xBE, (byte) 0xAA,
			(byte) 0x6B, (byte) 0xAA, (byte) 0xFB, (byte) 0xDC, (byte) 0x28,
			(byte) 0xB6, (byte) 0x48, (byte) 0xE1, (byte) 0xC7 };

	/**
	 * Default RSA private key exponent.
	 */
	public static final byte[] PRIV_KEY_EXP_BYTES = { (byte) 0x50, (byte) 0x9B,
			(byte) 0xF7, (byte) 0x28, (byte) 0x2A, (byte) 0x0F, (byte) 0x93,
			(byte) 0x29, (byte) 0x60, (byte) 0x23, (byte) 0x94, (byte) 0x67,
			(byte) 0x13, (byte) 0x3C, (byte) 0x37, (byte) 0xC8, (byte) 0xF8,
			(byte) 0x5E, (byte) 0xC7, (byte) 0x38, (byte) 0xF6, (byte) 0x3F,
			(byte) 0x87, (byte) 0xD2, (byte) 0x8D, (byte) 0xF6, (byte) 0x6B,
			(byte) 0x2F, (byte) 0x4B, (byte) 0x4D, (byte) 0x24, (byte) 0x09,
			(byte) 0x43, (byte) 0xC4, (byte) 0xBD, (byte) 0x44, (byte) 0x21,
			(byte) 0x3B, (byte) 0x66, (byte) 0x2C, (byte) 0xEE, (byte) 0x61,
			(byte) 0x3B, (byte) 0x17, (byte) 0x19, (byte) 0x60, (byte) 0xB0,
			(byte) 0x38, (byte) 0xE5, (byte) 0x79, (byte) 0xEB, (byte) 0x62,
			(byte) 0xD4, (byte) 0x8B, (byte) 0x5B, (byte) 0x76, (byte) 0x0F,
			(byte) 0x9B, (byte) 0xD0, (byte) 0x9A, (byte) 0x7C, (byte) 0xC8,
			(byte) 0x20, (byte) 0x5E, (byte) 0xA2, (byte) 0xCB, (byte) 0x19,
			(byte) 0xF8, (byte) 0xCB, (byte) 0x8A, (byte) 0xC2, (byte) 0x3B,
			(byte) 0x2A, (byte) 0xA2, (byte) 0x59, (byte) 0xF6, (byte) 0x21,
			(byte) 0xA3, (byte) 0x7F, (byte) 0x16, (byte) 0xCD, (byte) 0xA5,
			(byte) 0x54, (byte) 0xFD, (byte) 0x85, (byte) 0x5B, (byte) 0x6A,
			(byte) 0x58, (byte) 0x85, (byte) 0xC1, (byte) 0xB8, (byte) 0x4A,
			(byte) 0xE8, (byte) 0xC2, (byte) 0x49, (byte) 0x01, (byte) 0x43,
			(byte) 0xA3, (byte) 0x1F, (byte) 0xD0, (byte) 0x65, (byte) 0xD2,
			(byte) 0xB8, (byte) 0x66, (byte) 0x51, (byte) 0x50, (byte) 0xA8,
			(byte) 0x7F, (byte) 0xDB, (byte) 0x19, (byte) 0x34, (byte) 0x9D,
			(byte) 0x26, (byte) 0x00, (byte) 0x08, (byte) 0xCB, (byte) 0xB9,
			(byte) 0x4A, (byte) 0x6E, (byte) 0xBD, (byte) 0x1E, (byte) 0x89,
			(byte) 0x07, (byte) 0x14, (byte) 0xEB, (byte) 0x07, (byte) 0xD6,
			(byte) 0x48, (byte) 0x6D, (byte) 0x11, (byte) 0xA8, (byte) 0x14,
			(byte) 0xC3, (byte) 0xB3, (byte) 0x14, (byte) 0x2A, (byte) 0x63,
			(byte) 0x63, (byte) 0x51, (byte) 0x0C, (byte) 0xF5, (byte) 0x93,
			(byte) 0x1C, (byte) 0x94, (byte) 0x73, (byte) 0xD3, (byte) 0x7B,
			(byte) 0x21, (byte) 0xA3, (byte) 0xE7, (byte) 0x57, (byte) 0x22,
			(byte) 0xC1, (byte) 0x60, (byte) 0x62, (byte) 0xB9, (byte) 0xBA,
			(byte) 0x2E, (byte) 0x40, (byte) 0xFC, (byte) 0xF3, (byte) 0x35,
			(byte) 0x1F, (byte) 0xEC, (byte) 0x7C, (byte) 0x10, (byte) 0xE7,
			(byte) 0x27, (byte) 0x13, (byte) 0x1C, (byte) 0x32, (byte) 0x4C,
			(byte) 0xA1, (byte) 0x58, (byte) 0x85, (byte) 0x5D, (byte) 0xEB,
			(byte) 0x0D, (byte) 0xFC, (byte) 0x91, (byte) 0x5B, (byte) 0xD4,
			(byte) 0xFF, (byte) 0x46, (byte) 0xF4, (byte) 0x8F, (byte) 0x2A,
			(byte) 0x98, (byte) 0x05, (byte) 0x1D, (byte) 0xE6, (byte) 0xAE,
			(byte) 0xCC, (byte) 0x24, (byte) 0xFE, (byte) 0xD7, (byte) 0xCC,
			(byte) 0x40, (byte) 0xE7, (byte) 0xF9, (byte) 0x22, (byte) 0xD0,
			(byte) 0x02, (byte) 0x29, (byte) 0xA5, (byte) 0x65, (byte) 0x7A,
			(byte) 0x54, (byte) 0x9A, (byte) 0xDB, (byte) 0xCC, (byte) 0x4C,
			(byte) 0x83, (byte) 0x59, (byte) 0xD6, (byte) 0xDB, (byte) 0xE8,
			(byte) 0x8C, (byte) 0xD9, (byte) 0xE1, (byte) 0x75, (byte) 0x57,
			(byte) 0x43, (byte) 0xAB, (byte) 0xDC, (byte) 0x66, (byte) 0x80,
			(byte) 0xA1, (byte) 0x9D, (byte) 0x9B, (byte) 0x5B, (byte) 0xBC,
			(byte) 0xB1, (byte) 0x0C, (byte) 0x84, (byte) 0x30, (byte) 0xDF,
			(byte) 0x91, (byte) 0x48, (byte) 0xA4, (byte) 0xA3, (byte) 0x5D,
			(byte) 0xF9, (byte) 0xEF, (byte) 0x9F, (byte) 0xFF, (byte) 0x28,
			(byte) 0xA5, (byte) 0xA9, (byte) 0x28, (byte) 0x00, (byte) 0xFE,
			(byte) 0xDA, (byte) 0xD0, (byte) 0x4A, (byte) 0xE1 };

	/**
	 * Default AES data key
	 */
	public static final byte[] AES_DATA_KEY_BYTES = { (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31,
			(byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, (byte) 0x31, };

	/**
	 * Default IDTECH data key
	 */
	public static final byte[] IDTECH_DATA_KEY_BYTES = { (byte) 0x82,
			(byte) 0xDF, (byte) 0x8A, (byte) 0xC0, (byte) 0x22, (byte) 0x91,
			(byte) 0x62, (byte) 0xAF, (byte) 0x04, (byte) 0x0C, (byte) 0xF4,
			(byte) 0xD0, (byte) 0x76, (byte) 0x43, (byte) 0x72, (byte) 0x79, };

	/**
	 * Decrypt RSA block to decode track data information.
	 * 
	 * @param data
	 *            the RSA block.
	 * @return the String array that contains:
	 *         <ul>
	 *         <li>track 2</li>
	 *         <li>cardholder name</li>
	 *         </ul>
	 * @throws Exception
	 *             if an error occurs.
	 */
	public static String[] decryptTrackDataRSA(byte[] data) throws Exception {
		byte[] rsaBlock = new byte[256];
		System.arraycopy(data, 0, rsaBlock, 0, rsaBlock.length);
		byte[] decrypted = CryptoUtil.decryptRSABlock(PRIV_KEY_MOD_BYTES,
				PRIV_KEY_EXP_BYTES, rsaBlock);

		byte[] key = new byte[16];
		System.arraycopy(decrypted, 207, key, 0, key.length);

		byte[] enc = new byte[data.length - 272];
		System.arraycopy(data, 272, enc, 0, enc.length);

		byte[] dec = CryptoUtil.decryptAESCBC(key, enc);
		int offs = 0;

		String[] result = new String[2];

		result[0] = "";
		for (offs = 4; offs < dec.length && dec[offs] != 0; offs++) {
			result[0] += (char) dec[offs];
		}

		result[1] = "";
		for (offs += 1; offs < dec.length && dec[offs] != 0; offs++) {
			result[1] += (char) dec[offs];
		}

		offs++;
		byte[] sha256 = new byte[32];
		System.arraycopy(dec, offs, sha256, 0, sha256.length);

		MessageDigest md = MessageDigest.getInstance("SHA-256");
		md.update(data, 256, 16);
		md.update(dec, 0, offs);
		byte[] calcSHA = md.digest();

		if (!MessageDigest.isEqual(calcSHA, sha256)) {
			throw new RuntimeException("Wrong key");
		}

		return result;
	}

	/**
	 * Decrypt AES encrypted block to decode track data information.
	 * 
	 * @param data
	 *            the AES block.
	 * @return the String array that contains:
	 *         <ul>
	 *         <li>random number</li>
	 *         <li>serial number</li>
	 *         <li>track 1</li>
	 *         <li>track 2</li>
	 *         <li>track 3</li>
	 *         </ul>
	 * @throws Exception
	 *             if an error occurs.
	 */
	public static String[] decryptAESBlock(byte[] data) {
		byte[] decrypted = CryptoUtil.decryptAESCBC(AES_DATA_KEY_BYTES, data);
		if (decrypted == null) {
			throw new RuntimeException("Failed to decrypt");
		}

		int offset = 0;
		String[] result = new String[5];
		result[0] = HexUtil.byteArrayToHexString(decrypted, offset, 4);
		offset += 4;
		result[1] = HexUtil.byteArrayToHexString(decrypted, offset, 16);
		offset += 16;

		int index = -1;
		while (offset < decrypted.length && decrypted[offset] != 0) {
			int c = decrypted[offset] & 0xff;

			switch (c) {
			case 0xF1: {
				index = 2;
				result[index] = "";
				break;
			}
			case 0xF2: {
				index = 3;
				result[index] = "";
				break;
			}
			case 0xF3: {
				index = 4;
				result[index] = "";
				break;
			}
			default: {
				if (index != -1) {
					result[index] += (char) c;
				}
			}
			}
			offset++;
		}

		if (offset >= (decrypted.length - 2)
				|| decrypted[offset] != 0
				|| AudioReader.crcccit(decrypted, 0, offset + 1) != (((decrypted[offset + 1] & 0xff) << 8))
						+ (decrypted[offset + 2] & 0xff)) {
			throw new RuntimeException("Wrong key");
		}

		return result;
	}

	/**
	 * Decrypt IDTECH encrypted block to decode track data information.
	 * 
	 * @param data
	 *            the IDTECH block.
	 * @return the String array that contains:
	 *         <ul>
	 *         <li>card type</li>
	 *         <li>track 1</li>
	 *         <li>track 2</li>
	 *         <li>track 3</li>
	 *         </ul>
	 * @throws Exception
	 *             if an error occurs.
	 */
	public static String[] decryptIDTECHBlock(byte[] data) {
		/*
		 * DATA[0]: CARD TYPE: 0x0 - payment card DATA[1]: TRACK FLAGS DATA[2]:
		 * TRACK 1 LENGTH DATA[3]: TRACK 2 LENGTH DATA[4]: TRACK 3 LENGTH
		 * DATA[??]: TRACK 1 DATA MASKED DATA[??]: TRACK 2 DATA MASKED DATA[??]:
		 * TRACK 3 DATA DATA[??]: TRACK 1 AND TRACK 2 TDES ENCRYPTED DATA[??]:
		 * TRACK 1 SHA1 (0x14 BYTES) DATA[??]: TRACK 2 SHA1 (0x14 BYTES)
		 * DATA[??]: DUKPT SERIAL AND COUNTER (0x0A BYTES)
		 */
		int cardType = data[0] & 0xff;
		int track1Len = data[2] & 0xff;
		int track2Len = data[3] & 0xff;
		int track3Len = data[4] & 0xff;
		int offset = 5;

		String[] result = new String[4];
		result[0] = (cardType == 0) ? "PAYMENT" : "UNKNOWN";

		if (track1Len > 0) {
			offset += track1Len;
		}

		if (track2Len > 0) {
			offset += track2Len;
		}

		if (track3Len > 0) {
			result[3] = new String(data, offset, track3Len);
			offset += track3Len;
		}

		if ((track1Len + track2Len) > 0) {
			int blockSize = (track1Len + track2Len + 7) & 0xFFFFFF8;
			byte[] encrypted = new byte[blockSize];
			System.arraycopy(data, offset, encrypted, 0, encrypted.length);
			offset += blockSize;

			byte[] track1Hash = new byte[20];
			System.arraycopy(data, offset, track1Hash, 0, track1Hash.length);
			offset += track1Hash.length;

			byte[] track2Hash = new byte[20];
			System.arraycopy(data, offset, track2Hash, 0, track2Hash.length);
			offset += track2Hash.length;

			byte[] ipek = IDTECH_DATA_KEY_BYTES;
			byte[] ksn = new byte[10];
			System.arraycopy(data, offset, ksn, 0, ksn.length);
			offset += ksn.length;

			byte[] dataKey = CryptoUtil.calculateDataKey(ksn, ipek);
			byte[] decrypted = CryptoUtil.decrypt3DESCBC(dataKey, encrypted);

			if (decrypted == null)
				throw new RuntimeException("Failed to decrypt");

			if (track1Len > 0) {
				byte[] calcHash = CryptoUtil.calculateSHA1(decrypted, 0,
						track1Len);
				if (!Arrays.equals(track1Hash, calcHash)) {
					throw new RuntimeException("Wrong key");
				}
			}

			if (track2Len > 0) {
				byte[] calcHash = CryptoUtil.calculateSHA1(decrypted,
						track1Len, track2Len);
				if (!Arrays.equals(track2Hash, calcHash)) {
					throw new RuntimeException("Wrong key");
				}
			}

			if (track1Len > 0) {
				result[1] = new String(decrypted, 0, track1Len);
			}

			if (track2Len > 0) {
				result[2] = new String(decrypted, track1Len, track2Len);
			}
		}

		return result;
	}

	static String track2;
	final static String amount = "500";

	public static FinancialCard readCard(final PrimaProActivity a,
			AudioReaderTask t) throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		a.log("� Get financial card information\n", true);
		t.updateProgress("Please, present card");

		// Set parameters according to card reader.
		reader.setMagneticCardMode(AudioReader.ENCRYPTION_TYPE_AES256,
				AudioReader.TRACK_READ_MODE_ALLOWED,
				AudioReader.TRACK_READ_MODE_ALLOWED,
				AudioReader.TRACK_READ_MODE_ALLOWED);

		// Set parameters according to mask data.
		reader.setMagneticCardMaskMode(true, 6, 4);

		// Wait for valid card
		CardInfo cardInfo = reader.waitForCard(10000);
		t.updateProgress("Processing card...");
		FinancialCard fc = null;
		if (cardInfo.cardType == AudioReader.CARD_TYPE_MAGNETIC) {
			fc = reader.getFinancialCardData();

		} else if (cardInfo.cardType == AudioReader.CARD_TYPE_SMART_CARD) {
			a.log("  Smart card ATR: "
					+ HexUtil.byteArrayToHexString(cardInfo.atr) + "\n");

		} else if (cardInfo.cardType == AudioReader.CARD_TYPE_RFID) {
			ContactlessCard card = cardInfo.contactlessCard;

			a.log("  RFID card detected\n");
			processContactlessCard(a, t, card);

			a.log("� Remove card\n", true);
			card.remove();
		}

		a.log("� Power off reader\n", true);
		reader.powerOff();
		return fc;
	}

	private static void processContactlessCard(PrimaProActivity a,
			AudioReaderTask t, ContactlessCard contactlessCard)
			throws IOException {
		if (contactlessCard instanceof ISO14443Card) {
			ISO14443Card card = (ISO14443Card) contactlessCard;
			a.log("  ISO14 card: " + HexUtil.byteArrayToHexString(card.uid)
					+ "\n");

			// 16 bytes reading and 16 bytes writing
			// Try to authenticate first with default key
			byte[] key = new byte[] { -1, -1, -1, -1, -1, -1 };
			// It is best to store the keys you are going to use once in the
			// device memory,
			// then use AuthByLoadedKey function to authenticate blocks rather
			// than having the key in your program
			try {
				a.log("  Authenticate: " + HexUtil.byteArrayToHexString(key)
						+ "\n");
				card.authenticate('A', 8, key);
			} catch (RFIDException e) {
				a.logWarning("  Authenticate failed: " + e.getMessage() + "\n");
			}

			// Write data to the card
			byte[] input = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
					0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
			try {
				a.log("  Write data: " + HexUtil.byteArrayToHexString(input)
						+ "\n");
				card.write16(8, input);
			} catch (RFIDException e) {
				e.printStackTrace();
				a.logWarning("  Write failed: " + e.getMessage() + "\n");
			}

			// Read data from card
			try {
				byte[] result = card.read16(8);
				a.log("  Read data: " + HexUtil.byteArrayToHexString(result)
						+ "\n");
			} catch (RFIDException e) {
				a.logWarning("  Read card failed: " + e.getMessage() + "\n");
			}
		} else if (contactlessCard instanceof ISO15693Card) {
			ISO15693Card card = (ISO15693Card) contactlessCard;

			a.log("  ISO15 card: " + HexUtil.byteArrayToHexString(card.uid)
					+ "\n");
			a.log("  Block size: " + card.blockSize + "\n");
			a.log("  Max blocks: " + card.maxBlocks + "\n");

			if (card.blockSize > 0) {
				try {
					byte[] security = card.getBlocksSecurityStatus(0, 16);
					a.log("  Security status: "
							+ HexUtil.byteArrayToHexString(security) + "\n");
				} catch (RFIDException e) {
					a.logWarning("  Get security status failed: "
							+ e.getMessage() + "\n");
				}

				// Write data to the card
				byte[] input = new byte[] { 0x00, 0x01, 0x02, 0x03 };
				try {
					a.log("  Write data: "
							+ HexUtil.byteArrayToHexString(input) + "\n");
					card.write(0, input);
				} catch (RFIDException e) {
					a.logWarning("  Write failed: " + e.getMessage() + "\n");
				}

				// Read data from card
				try {
					byte[] result = card.read(0, 1);
					a.log("  Read data: "
							+ HexUtil.byteArrayToHexString(result) + "\n");
				} catch (RFIDException e) {
					a.log("  Read failed: " + e.getMessage() + "\n");
				}
			}
		} else if (contactlessCard instanceof FeliCaCard) {
			FeliCaCard card = (FeliCaCard) contactlessCard;

			a.log("  FeliCa card: " + HexUtil.byteArrayToHexString(card.uid)
					+ "\n");

			// Write data to the card
			byte[] input = new byte[] { 0x00, 0x01, 0x02, 0x03, 0x04, 0x05,
					0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F };
			try {
				a.log("  Write data: " + HexUtil.byteArrayToHexString(input)
						+ "\n");
				card.write(0x0900, 0, input);
			} catch (RFIDException e) {
				e.printStackTrace();
				a.logWarning("  Write failed: " + e.getMessage() + "\n");
			}

			// Read data from card
			try {
				byte[] result = card.read(0x0900, 0, 1);
				a.log("  Read data: " + HexUtil.byteArrayToHexString(result)
						+ "\n");
			} catch (RFIDException e) {
				e.printStackTrace();
				a.log("  Read failed: " + e.getMessage() + "\n");
			}

		} else if (contactlessCard instanceof STSRICard) {
			STSRICard card = (STSRICard) contactlessCard;

			a.log("  STSRI card: " + HexUtil.byteArrayToHexString(card.uid)
					+ "\n");
			a.log("  Block size: " + card.blockSize + "\n");

			// Write data to the card
			byte[] input = new byte[] { 0x00, 0x01, 0x02, 0x03 };
			try {
				card.writeBlock(8, input);
			} catch (RFIDException e) {
				a.logWarning("  Write failed: " + e.getMessage() + "\n");
			}

			// Try reading two blocks
			try {
				byte[] result = card.readBlock(8);
				a.log("  Read data: " + HexUtil.byteArrayToHexString(result)
						+ "\n");
			} catch (RFIDException e) {
				a.logWarning("  Read failed: " + e.getMessage() + "\n");
			}
		} else {
			a.log("  Contactless card: "
					+ HexUtil.byteArrayToHexString(contactlessCard.uid));
		}
	}

	public static void readInfo(PrimaProActivity a, AudioReaderTask t)
			throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		a.log("� Get device information\n", true);
		Identification ident = reader.getIdent();
		String serialNumber = reader.getSerialNumber();
		Battery battery = null;

		try {
			battery = reader.getBattery();
		} catch (AudioReaderException e) {
			// Skip exception if device does not support battery indication
			if (e.getStatusCode() != AudioReader.STATUS_INVALID_COMMAND_TYPE) {
				throw e;
			}
		}

		a.log("  Device : " + ident.name + "\n");
		a.log("  Version: " + ident.version + "\n");
		a.log("  Serial : " + serialNumber + "\n");
		if (battery != null) {
			a.log("  Battery: " + (double) battery.voltage / 1000 + "V ("
					+ battery.level + "%)\n");
		}

		a.log("� Power off reader\n", true);
		reader.powerOff();
	}

	public static void sendAPDU(PrimaProActivity a, AudioReaderTask t)
			throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		final byte[] KEK = new byte[] { 0x11, 0x11, 0x11, 0x11, 0x11, 0x11,
				0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11 };
		final byte[] IPEK = new byte[] { (byte) 0x82, (byte) 0xDF, (byte) 0x8A,
				(byte) 0xC0, 0x22, (byte) 0x91, 0x62, (byte) 0xAF, 0x04, 0x0C,
				(byte) 0xF4, (byte) 0xD0, 0x76, 0x43, 0x72, 0x79 };
		final byte[] KSN = new byte[] { 0x32, 0x01, 0x10, 0x00, 0x00, 0x00,
				0x00, 0x00, 0x00, 0x00 };
		final byte[] IV = new byte[] { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
				0x00, 0x00 };

		a.log("� Loading new keys\n", true);
		int kekVer = reader.kmGetKEKVersion();
		int ipekVer = reader.kmGetIPEKVersion();

		// Load KEK
		if (kekVer == 0) {
			ByteBuffer buffer = ByteBuffer.allocate(4 + 16);
			buffer.order(ByteOrder.BIG_ENDIAN);
			buffer.putInt(kekVer + 1);
			buffer.put(KEK);

			reader.kmLoadKEK(buffer.array());
		} else {
			ByteBuffer buffer = ByteBuffer.allocate(4 + 16 + 32 + 4);
			buffer.order(ByteOrder.BIG_ENDIAN);
			buffer.putInt(kekVer + 1);
			buffer.put(KEK);
			byte[] hash = CryptoUtil.calculateSHA256(buffer.array(), 0,
					buffer.position());
			buffer.put(hash);

			byte[] encrypted = CryptoUtil.encrypt3DESCBC(KEK, new byte[8],
					buffer.array());
			reader.kmLoadKEK(encrypted);
		}

		// Load IPEK
		if (ipekVer == 0) {
			ByteBuffer buffer = ByteBuffer.allocate(4 + 16 + 10);
			buffer.order(ByteOrder.BIG_ENDIAN);
			buffer.putInt(ipekVer + 1);
			buffer.put(IPEK);
			buffer.put(KSN);

			reader.kmLoadIPEK(0, buffer.array());
		} else {
			ByteBuffer buffer = ByteBuffer.allocate(4 + 16 + 10 + 32 + 2);
			buffer.order(ByteOrder.BIG_ENDIAN);
			buffer.putInt(ipekVer + 1);
			buffer.put(IPEK);
			buffer.put(KSN);
			byte[] hash = CryptoUtil.calculateSHA256(buffer.array(), 0,
					buffer.position());
			buffer.put(hash);

			byte[] encrypted = CryptoUtil.encrypt3DESCBC(KEK, new byte[8],
					buffer.array());
			reader.kmLoadIPEK(0, encrypted);
		}

		a.log("� Get key version\n", true);
		kekVer = reader.kmGetKEKVersion();
		ipekVer = reader.kmGetIPEKVersion();
		a.log("  KEK version: " + kekVer + "\n");
		a.log("  IPEK version: " + ipekVer + "\n");

		a.log("� Power on smart card\n", true);
		reader.powerOnSmartCard();

		a.log("� Get KSN and derivate data key\n", true);
		byte[] ksn = reader.kmGetKSN();
		byte[] key = CryptoUtil.calculateDataKey(ksn, IPEK);
		reader.kmSetInitialVector(IV);
		a.log("  IPEK: " + HexUtil.byteArrayToHexString(IPEK) + "\n");
		a.log("  KSN: " + HexUtil.byteArrayToHexString(ksn) + "\n");
		a.log("  Key: " + HexUtil.byteArrayToHexString(key) + "\n");

		byte[] requestAPDU = new byte[] { (byte) 0x00, (byte) 0x84, (byte) 0,
				(byte) 0, (byte) 0 };
		ByteBuffer buffer = ByteBuffer
				.allocate((2 + requestAPDU.length + 2 + 7) & 0xFFFFFFF8);
		buffer.order(ByteOrder.BIG_ENDIAN);
		buffer.putShort((short) requestAPDU.length);
		buffer.put(requestAPDU);
		int crc = AudioReader.crcccit(buffer.array(), 0, buffer.position());
		buffer.putShort((short) crc);
		byte[] encrypted = CryptoUtil.encrypt3DESCBC(key, IV, buffer.array());

		a.log("� Transmit APDU\n", true);
		byte[] result = reader.transmitAPDU(0, encrypted);
		byte[] decrypted = CryptoUtil.decrypt3DESCBC(key, IV, result);

		a.log("� Descrypt response data\n", true);
		a.log("  RequestAPDU: " + HexUtil.byteArrayToHexString(requestAPDU)
				+ "\n");
		// Check decrypted information
		int len = ((decrypted[0] & 0xff) << 8) + (decrypted[1] & 0xff);
		if (len <= (decrypted.length - 2)
				&& AudioReader.crcccit(decrypted, 0, 2 + len) == (((decrypted[len + 2] & 0xff) << 8) + (decrypted[len + 2 + 1] & 0xff))) {
			byte[] data = new byte[len];
			System.arraycopy(decrypted, 2, data, 0, data.length);
			a.log("  ResponseAPDU: " + HexUtil.byteArrayToHexString(data)
					+ "\n");
		} else {
			a.logWarning("  Decryption error\n");
		}

		a.log("� Power off reader\n", true);
		reader.powerOff();
	}

	public static void loadKeys(PrimaProActivity a, AudioReaderTask t)
			throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		a.log("� Get RSA key information\n", true);
		DeviceKeysInfo keyInfo = reader.kmGetKeysInfo();
		DeviceKey deviceKey = keyInfo.getDeviceKey(
				AudioReader.KEY_TYPE_RSA2048_DATA, 0);
		a.log("  Name: " + deviceKey.name + "\n");
		a.log("  Version: " + deviceKey.version + "\n");

		byte[] signature = CryptoUtil.signSHA256withRSA(PRIV_KEY_MOD_BYTES,
				PRIV_KEY_EXP_BYTES, PRIV_KEY_MOD_BYTES);
		int newKeyVersion = deviceKey.version + 1;
		ByteArrayOutputStream o = new ByteArrayOutputStream();
		o.write(signature);
		o.write(newKeyVersion >> 24);
		o.write(newKeyVersion >> 16);
		o.write(newKeyVersion >> 8);
		o.write(newKeyVersion);
		o.write(PRIV_KEY_MOD_BYTES);

		a.log("� Load new RSA key\n", true);
		byte[] rsaBlock = o.toByteArray();
		reader.kmLoadRSAKey(rsaBlock, 0);
		a.log("  Version: " + newKeyVersion + "\n");

		a.log("� Generate new session key\n", true);
		reader.kmGenerateRSASymmetricKey(AudioReader.KEY_TYPE_AES128_DATA, 0);

		a.log("� Power off reader\n", true);
		reader.powerOff();
	}

	public static void updateFirmware(PrimaProActivity a, AudioReaderTask t,
			FirmwareInformation fi) throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		a.log("� Start firmware update\n", true);
		a.log("  Firmware: " + fi.name + "\n");
		a.log("  Version: " + fi.version + "\n");

		final int[] lastPercent = { 0 };
		final AudioReaderTask task = t;
		reader.updateFirmware(fi, new OnUpdateFirmwareListener() {
			@Override
			public boolean onDataSend(int bytesSent, int totalBytes) {
				int percent = (bytesSent * 100) / totalBytes;

				if (percent != lastPercent[0]) {
					task.updateProgress("Update firmware (" + percent + "%)...");
					lastPercent[0] = percent;
				}

				return task.isCanceled();
			}
		});

		if (task.isCanceled()) {
			a.logWarning("  Firmware update is canceled\n");
		} else {
			a.log("  Firmware update completed successful\n");
		}
		a.log("� Power off reader\n", true);
		reader.powerOff();
	}

	public static void checkDevice(PrimaProActivity a, AudioReaderTask t)
			throws Exception {
		final AudioReader reader = t.getReader();

		a.log("� Power on reader\n", true);
		reader.powerOn();

		a.log("� Get device information\n", true);
		Identification ident = reader.getIdent();
		a.log("  Device : " + ident.name.trim() + " (" + ident.version + ")\n");

		a.log("� Check device connection\n", true);
		for (int i = 32; i <= 256; i += 32) {
			byte[] data = new byte[i];

			for (int j = 0; j < data.length; j++) {
				data[j] = (byte) j;
			}

			try {
				reader.test(data);
				a.log("  Transmit " + data.length + " bytes... OK ("
						+ reader.getLastRetries() + ")\n");
			} catch (IOException e) {
				a.log("  Transmit " + data.length + " bytes... FAILED ("
						+ reader.getLastRetries() + ")\n");
				throw e;
			}
		}

		a.log("� Power off reader\n", true);
		reader.powerOff();
	}

}
