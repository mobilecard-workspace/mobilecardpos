package org.addcel.datecs.activities;

import java.io.IOException;

import org.addcel.mobilecardpos.views.PrimaProActivity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnKeyListener;
import android.view.KeyEvent;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReaderException;
import com.datecs.audioreader.AudioReaderManager;

public abstract class AudioReaderTask extends Thread {
    private Activity mActivity;
    private ProgressDialog mDialog;
    private AudioReader mAudioReader;
    private boolean mCanceled;
    
    public AudioReaderTask(PrimaProActivity activity, boolean isCancelable) {
        this.mActivity = activity;            
        this.mDialog = new ProgressDialog(activity);        
        this.mDialog.setMessage("operation_in_progress"); 
        this.mDialog.setCancelable(false); 
        this.mDialog.setCanceledOnTouchOutside(false);
        this.mDialog.setOnKeyListener(new OnKeyListener() { 
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) { 
                return true; // prevent from close dialog
            }
        });
        
        if (isCancelable) {
            this.mDialog.setButton(AlertDialog.BUTTON_NEGATIVE,
                    activity.getString(android.R.string.cancel),
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mCanceled = true;
                    
                    if (mAudioReader != null) {
                        try {                            
                            mAudioReader.cancel();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }           
            });             
        }
        
        this.mDialog.show();
    }
      
    public abstract void onExecute() throws Exception;        
    
    public abstract void onFinish(long ellapse);    
    
    public abstract void onError(Exception e);
    
    @Override
    public void run() {
        Context context = mActivity.getApplicationContext();
        
        synchronized (context) {
            try {
                // Create audio reader instance.
                mAudioReader = AudioReaderManager.getReader(context);
                
                // Get start time of execution
                long start = System.currentTimeMillis();
                
                // Perform execution
                onExecute();
                
                // Calculate execution time.
                long ellapse = System.currentTimeMillis() - start;
                     
                onFinish(ellapse);
            } catch (AudioReaderException e) {                               
                // Power on reader when protocol exception occurs. 
                try {
                    mAudioReader.powerOff();
                } catch (IOException e2) { }                                    
                onError(e);
            } catch (Exception e) {
                onError(e);
            } finally {
                if (mAudioReader != null) {
                    mAudioReader.close();
                    mAudioReader = null;
                }
                mDialog.dismiss();
            }                   
        }   
    }        

    public boolean isCanceled() {
        return this.mCanceled;
    }
    
    public AudioReader getReader() {
        return this.mAudioReader;
    }
    
    public Dialog getProgressDialog() {
        return this.mDialog;
    }

    public void updateProgress(String message) {
        final String msg = message;
        
        mActivity.runOnUiThread(new Runnable() {            
            @Override
            public void run() {
                mDialog.setMessage(msg);
            }
        });
    }
}