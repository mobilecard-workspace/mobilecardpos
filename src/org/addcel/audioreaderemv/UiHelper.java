package org.addcel.audioreaderemv;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.text.Spannable;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.View;
import android.widget.ScrollView;
import android.widget.TextView;

public abstract class UiHelper {

    public static void appendColorText(final TextView textView, final ScrollView scrollView, final String text, final int color, final boolean bold) {
        System.out.println("-> " + text);
        
        textView.post(new Runnable() {          
            @Override
            public void run() {
                int start = textView.getText().length();
                textView.append(text);
                int end = textView.getText().length();
                Spannable spannableText = (Spannable) textView.getText();
                spannableText.setSpan(new ForegroundColorSpan(color), start, end, 0);
                if (bold) {
                    spannableText.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), start, end, 0);
                }               
                
                if (scrollView != null) {
                    scrollView.post(new Runnable() {            
                        @Override
                        public void run() {
                            scrollView.fullScroll(View.FOCUS_DOWN);
                        }
                    });
                }
            }
        });
    }

    public static Dialog createSelectionDialog(Activity activity, int resTitle, String[] list, final Integer[] result) {        
        final AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle(resTitle);
        builder.setCancelable(false);       
        builder.setSingleChoiceItems(list, result[0], new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                result[0] = whichButton;              
            }
        });
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {                
                synchronized (result) {
                    result.notify();                    
                }                
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) { 
                result[0] = -1;                
                synchronized (result) {
                    result.notify();
                }
            }
        });          
        return builder.create(); 
    }

    public static int selectDialogItemOnThread(final Activity activity, final int resTitle, final String[] list) {
        final Integer[] result = { 0 };
        
        activity.runOnUiThread(new Runnable() {            
            @Override
            public void run() {
                Dialog dialog = createSelectionDialog(activity, resTitle, list, result);         
                dialog.show();
            }
        });
        
        synchronized(result) {
            try {
                result.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        
        return result[0];
    }
    
}
