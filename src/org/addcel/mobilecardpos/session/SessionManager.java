package org.addcel.mobilecardpos.session;

import org.addcel.mobilcardpos.util.Constructores;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class SessionManager {
	private SharedPreferences pref;
	private Editor editor;
	private Context _context;

	//private int PRIVATE_MODE = Context.MODE_PRIVATE;
	private int PUBLIC_MODE = Context.MODE_MULTI_PROCESS;

	@SuppressLint("CommitPrefEdits")
	public SessionManager(Context context) {
		this._context = context;
		pref = _context.getSharedPreferences(Constructores.PREF_NAME,
				PUBLIC_MODE);
		editor = pref.edit();
	}

	/**
	 * @return the latitud
	 */
	public String getLatitud() {
		return pref.getString(Constructores.LATITUD, null);
	}

	/**
	 * @param latitud
	 *            the latitud to set
	 */
	public void setLatitud(String latitud) {
		editor.putString(Constructores.LATITUD, latitud);
		editor.commit();
	}

	/**
	 * @return the longitud
	 */
	public String getLongitud() {
		return pref.getString(Constructores.LONGITUD, null);
	}

	/**
	 * @param longitud
	 *            the longitud to set
	 */
	public void setLongitud(String longitud) {
		editor.putString(Constructores.LONGITUD, longitud);
		editor.commit();
	}

	public void setTime_GPS(int interv){
		editor.putInt(Constructores.TIEMPO_NOTIFICACION, interv);
		editor.commit();
	}
	public int getTime_GPS() {
		return pref.getInt(Constructores.TIEMPO_NOTIFICACION, 0);
	}

	public void setTime_GPS_ant(int interv) {
		editor.putInt(Constructores.TIEMPO_NOTIF_ANTERIOR, interv);
		editor.commit();
	}
	
	
	public int getTime_GPS_ant(){
		return pref.getInt(Constructores.TIEMPO_NOTIF_ANTERIOR, 0);
	}
	

}
