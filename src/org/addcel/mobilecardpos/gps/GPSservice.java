package org.addcel.mobilecardpos.gps;

import org.addcel.mobilecardpos.session.SessionManager;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class GPSservice extends Service {

	GPSTracker gps;
	private SessionManager session;
	private double latitude;
	private double longitude;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@Deprecated
	public void onStart(Intent intent, int startId) {
		// TODO Auto-generated method stub
		super.onStart(intent, startId);
		session = new SessionManager(getApplicationContext());
		String TAG = getApplicationContext().getPackageName().toString();

		gps = new GPSTracker(getApplicationContext());

		if (gps.canGetLocation()) {

			latitude = gps.getLatitud();
			longitude = gps.getLongitud();

			// \n is for new line

			// Toast.makeText( getApplicationContext(),
			// "Tu Locacion es - \nLat: " + latitude + "\nLong: " + longitude,
			// Toast.LENGTH_LONG).show();

			Log.i(TAG, "Ubicación generada: long " + longitude + " lati "
					+ latitude);

			session.setLatitud(Double.toString(latitude));
			session.setLongitud(Double.toString(longitude));
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

	}

}
