package org.addcel.mobilecardpos.views;

import java.util.ArrayList;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.datecs.activities.AudioReaderHelper;
import org.addcel.datecs.activities.AudioReaderTask;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilcardpos.util.Constructores;
import org.addcel.mobilcardpos.util.Urls;
import org.addcel.mobilecardpos.R;
import org.addcel.mobilecardpos.session.SessionManager;

import org.addcel.util.AppUtils;
import org.addcel.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.datecs.audioreader.AudioReader;
import com.datecs.audioreader.AudioReader.FinancialCard;
import com.datecs.audioreader.AudioReaderPlayer;
import com.datecs.audioreader.AudioReaderRecorder;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PrimaProActivity extends Activity {
	private static final String TAG = "AddcelPOSPrima";
	private Context contxt = PrimaProActivity.this;
	FinancialCard fc;
	String holder;
	String number;
	String year;
	String month;
	Button pagar;
	Button leerT;
	private SessionManager session;

	private abstract class MyAudioReaderTask extends AudioReaderTask {

		public MyAudioReaderTask(PrimaProActivity activity, boolean isCancelable) {
			super(activity, isCancelable);
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onFinish(long ellapse) {
			// TODO Auto-generated method stub
			log("\nOperaci�n completada en " + ((double) ellapse / 1000)
					+ "s\n");
			if (fc != null) {
				pintaDatos();	
			}
			// log("\n El PAN es: " + getNumeber(fc) +"\n\n", true);

		}

		@Override
		public void onError(Exception e) {
			// TODO Auto-generated method stub
			logError("\nFallo de  operaci�n: ", e);

		}

	}

	private class HeadsetReceiver extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			boolean headsetAvailable = intent.getIntExtra("state", 0) == 1;
			boolean microphoneAvailable = intent.getIntExtra("microphone", 0) == 1;
			boolean isConnected = headsetAvailable && microphoneAvailable;
			updateReaderState(isConnected);
		}

	}

	private final HeadsetReceiver mHeadsetReceiver = new HeadsetReceiver();
	private ScrollView mLogScrollView;
	private TextView mLogView;
	//private EditText editProd;
	private EditText editNombre;
	private EditText editMail;
	private EditText editYear;
	private EditText editMonth;
	private EditText editTipo;
	private EditText editNum;
	private EditText editTotal;
	private EditText editCVV2;

	private String monto;
	private String nombre;
	private String correo;
	private String cvv2;
//	private String descrip;
	private String vigencia;
	//private String tarjetaNo;
	private String mes;
	private String anio;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.primapro_pago_activity);

		TextView txtVersion = (TextView) findViewById(R.id.txt_version);
		txtVersion.setText(Build.MANUFACTURER + " " + Build.MODEL);
		session = new SessionManager(contxt);
		
		List<String> list = new ArrayList<String>();
		list.add("CREDITO");
		//list.add("DEBITO");
		Spinner spinner2 = (Spinner) findViewById(R.id.spinnerTipo);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);

		mLogScrollView = (ScrollView) findViewById(R.id.scroll_log);
		mLogView = (TextView) findViewById(R.id.txt_log);
		//mLogScrollView.setVisibility(View.GONE);

	//	editProd = (EditText) findViewById(R.id.productoPrima);
		editMail = (EditText) findViewById(R.id.correoPrima);
		editMonth = (EditText) findViewById(R.id.monthVigencia);
		editNombre = (EditText) findViewById(R.id.nombrePrima);
		editTipo = (EditText) findViewById(R.id.tipoTarjeta);
		editYear = (EditText) findViewById(R.id.yearVigencia);
		editNum = (EditText) findViewById(R.id.numeroTarjeta);
		editCVV2 = (EditText) findViewById(R.id.cvvTarjeta);
		editTotal = (EditText) findViewById(R.id.totalPrima);

		findViewById(R.id.btn_read_card).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						// TODO Auto-generated method stub
						clearLog();
						readCard();

					}
				});

		findViewById(R.id.btn_pagar).setOnClickListener(new OnClickListener() {
			String ERROR_MSG = "El campo no puede est�r vac�o";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//if (TextUtils.isEmpty(editProd.getText())) {
				//	editProd.setError(ERROR_MSG);
				//	Toast.makeText(contxt, "Campo Producto vacio",
				//			Toast.LENGTH_SHORT).show();
				//}
				 if (TextUtils.isEmpty(editNombre.getText())) {
					editNombre.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Nombre vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editMail.getText())) {
					editMail.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Coreo vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editCVV2.getText())) {
					editCVV2.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo CVV2 vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editTotal.getText())) {
					editTotal.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Total vacio",
							Toast.LENGTH_SHORT).show();
				} else {
					//descrip = editProd.getText().toString().trim();
					nombre = editNombre.getText().toString().trim();
					correo = editMail.getText().toString().trim();
					cvv2 = editCVV2.getText().toString().trim();
					// tarjetaNo = editNum.getText().toString().trim();
					mes = Integer.toString(fc.month);
					anio = Integer.toString(fc.year);
					if (mes.length() == 1) {
						vigencia = "0" + mes + "/" + anio;
					} else {
						vigencia = mes + "/" + anio;
					}

					monto = editTotal.getText().toString().trim();
					pedirToken();
				}
			}
		});
		// Set debugging options
		AudioReader.setDebug(true);
		AudioReaderPlayer.setDebug(false);
		AudioReaderRecorder.setDebug(false);
		AudioReaderRecorder.setLogFile(null);
		// AudioReaderRecorder.setLogFile(android.os.Environment.getExternalStorageDirectory().getPath()
		// + "/audioreader.txt");

		// Set sound channel
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		registerReceiver(mHeadsetReceiver, new IntentFilter(
				Intent.ACTION_HEADSET_PLUG));
		updateReaderState(false);
		findViewById(R.id.btn_pagar).setEnabled(false);
		findViewById(R.id.btn_pagar).setFocusable(true);
		findViewById(R.id.btn_pagar).setPressed(true);
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		unregisterReceiver(mHeadsetReceiver);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		startActivity(new Intent(PrimaProActivity.this,MenuInicioActivity.class));
		finish();
	}
	
	private void updateReaderState(boolean connected) {
		findViewById(R.id.btn_read_card).setEnabled(connected);
		findViewById(R.id.btn_read_card).setFocusable(!connected);
		findViewById(R.id.btn_read_card).setPressed(!connected);

		if (connected) {
			log("� Dispositivo Conectado\n", true);
			// readInfo();
		} else {
			clearLog();
			logWarning("� Dispositivo no Conectado\n");
		}
	}

	private void clearLog() {
		mLogView.post(new Runnable() {
			@Override
			public void run() {
				mLogView.setText("");
			}
		});
	}

	private void logText(final TextView textView, final ScrollView scrollView,
			final String text, final int color, final boolean bold) {
		textView.post(new Runnable() {
			@Override
			public void run() {
				int start = textView.getText().length();
				textView.append(text);
				int end = textView.getText().length();
				Spannable spannableText = (Spannable) textView.getText();
				spannableText.setSpan(new ForegroundColorSpan(color), start,
						end, 0);
				if (bold) {
					spannableText.setSpan(new StyleSpan(
							android.graphics.Typeface.BOLD), start, end, 0);
				}

				if (scrollView != null) {
					scrollView.post(new Runnable() {
						@Override
						public void run() {
							scrollView.fullScroll(View.FOCUS_DOWN);
						}
					});
				}
			}
		});
	}

	public void log(final String text) {
		log(text, false);
	}

	public void log(final String text, boolean bold) {
		if (bold) {
			Log.i(TAG, text);
		} else {
			Log.d(TAG, text);
		}
		logText(mLogView, mLogScrollView, text, Color.BLACK, bold);
	}

	public void logError(final String text) {
		logText(mLogView, mLogScrollView, text, Color.RED, true);
	}

	public void logError(final String text, Exception e) {
		Log.e(TAG, text, e);
		logError(text + e.getMessage() + "\n");
	}

	public void logWarning(final String text) {
		Log.w(TAG, text);
		logText(mLogView, mLogScrollView, text, 0xFFFF7F00, false);
	}

	protected void readCard() {
		MyAudioReaderTask task = new MyAudioReaderTask(this, false) {

			@Override
			public void onExecute() throws Exception {
				// TODO Auto-generated method stub
				fc = AudioReaderHelper.readCard(PrimaProActivity.this, this);
				holder = fc.holder;
				number = fc.number;
				year = Integer.toString(fc.year);
				month = Integer.toString(fc.month);
			}
		};

		task.start();

	}

	public void pintaDatos() {
		editTipo.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editTipo.setText(holder);
			}
		});
		editNum.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editNum.setText(number);
			}
		});
		editMonth.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editMonth.setText(month);
			}
		});
		editYear.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editYear.setText(year);
			}
		});

		if (!fc.number.isEmpty() && fc.year != 0) {
			findViewById(R.id.btn_pagar).post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					findViewById(R.id.btn_pagar).setEnabled(true);
					findViewById(R.id.btn_pagar).setFocusable(false);
					findViewById(R.id.btn_pagar).setPressed(false);
				}
			});
		}
	}

	private void pedirToken() {
		JSONObject json = new JSONObject();

		try {
			json.put(Constructores.USER, "userPrueba");
			json.put(Constructores.PASSWORD, "passwordPrueba");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private String token;

	private class TokenListener implements WSResponseListener {

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {
					JSONObject json = new JSONObject(response);
					token = json.getString(Constructores.TOKEN);
				} catch (JSONException e) {
					// TODO: handle exception
				}

				if (token != null) {
					Toast.makeText(contxt, "Token Exitoso", Toast.LENGTH_SHORT)
							.show();
					enviarPago();
				}
			}
		}

	}

	private void enviarPago() {
		JSONObject json = new JSONObject();

		try {
			json.put(Constructores.TOKEN, token);
			json.put(Constructores.CORREO, correo);
			json.put(Constructores.NOMBRE, nombre);
			json.put(Constructores.TARJETA, getNumeber(fc));
			json.put(Constructores.CVV2, cvv2);
			json.put(Constructores.VIGENCIA, vigencia);
			json.put(Constructores.PRODUCTO, "producto");
			json.put(Constructores.MONTO, monto);
			json.put(Constructores.IMEI, DeviceUtils.getIMEI(contxt));
			json.put(Constructores.TIPO_DISP, DeviceUtils.getTipo());
			json.put(Constructores.SOFTWARE, DeviceUtils.getSWVersion());
			json.put(Constructores.MODELO, DeviceUtils.getModel());
			json.put(Constructores.LATITUD, session.getLatitud().toString());
			json.put(Constructores.LONGITUD, session.getLongitud().toString());
			json.put(Constructores.KEY, DeviceUtils.getIMEI(contxt));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i(TAG, json.toString());
		new WebServiceClient(new PagoListener(), contxt, true, Urls.url_pago)
				.execute(AddcelCrypto.encryptSensitive(AppUtils.getKey(),
						json.toString()));
	}

	private class PagoListener implements WSResponseListener {
		private static final String TAG = "PagoListener";
		private String errorMsj;
		private int resultado;
		//private String addcelIR;
		private String mont;
		private String autoriz;

		@SuppressLint("NewApi")
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, "response: " + response);
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);
				
				JSONObject json = null;
				try {
					json = new JSONObject(response);
					resultado = json.getInt(Constructores.ERROR_ID);
					errorMsj = json.getString(Constructores.ERROR_MSG);
					//addcelIR = json.getString(Constructores.ADDCEL_IR);
					
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);
				
				if (resultado == 1) {
					
					try {
						autoriz = json.getString(Constructores.AUTORIZACION);
						mont = json.getString(Constructores.MONTO);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					builder.setTitle("Pago exitoso");
					builder.setMessage(errorMsj + "\nAutorizaci�n: " + autoriz
							+ "\nTotal: $ " + mont);

				} else {

					builder.setTitle("Error");
					builder.setMessage(errorMsj);

				}
				builder.setPositiveButton("ACEPTAR",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								startActivity(new Intent(contxt,
										MenuInicioActivity.class));
							}
						});
				AlertDialog aldialog = builder.create();
				aldialog.show();
			}
		}

	}

	private String getNumeber(FinancialCard fcard) {
		String track2 = null;
		String num = null;
		int encryptionType = (fcard.data[0] >>> 3);
		// Trim extract encrypted block.
		byte[] encryptedData = new byte[fcard.data.length - 1];
		System.arraycopy(fcard.data, 1, encryptedData, 0, encryptedData.length);

		if (encryptionType == AudioReader.ENCRYPTION_TYPE_OLD_RSA
				|| encryptionType == AudioReader.ENCRYPTION_TYPE_RSA) {
			// a.log("� Decrypt card data\n", true);
			try {
				String[] result = AudioReaderHelper
						.decryptTrackDataRSA(encryptedData);
				// a.log("  Track2: " + result[0] + "\n");
				// almacenar
				track2 = result[0];
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else if (encryptionType == AudioReader.ENCRYPTION_TYPE_AES256) {
			try {
				String[] result = AudioReaderHelper
						.decryptAESBlock(encryptedData);

				// a.log("  Random data: " + result[0] + "\n");
				// a.log("  Serial number: " + result[1] + "\n");
				if (result[2] != null) {
					// a.log("  Track1: " + result[2] + "\n");
				}
				if (result[3] != null) {
					// a.log("  Track2: " + result[3] + "\n");
					track2 = result[3];
				}
				if (result[4] != null) {
					// a.log("  Track3: " + result[4] + "\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else if (encryptionType == AudioReader.ENCRYPTION_TYPE_IDTECH) {
			try {
				String[] result = AudioReaderHelper
						.decryptIDTECHBlock(encryptedData);

				// a.log("  Card type: " + result[0] + "\n");
				if (result[1] != null) {
					// a.log("  Track1: " + result[1] + "\n");
				}
				if (result[2] != null) {
					// a.log("  Track2: " + result[2] + "\n");
					track2 = result[2];
				}
				if (result[3] != null) {
					// a.log("  Track3: " + result[3] + "\n");
				}
			} catch (Exception e) {
				e.printStackTrace();
				// a.logWarning("  Failed to decrypt data: " + e.getMessage() +
				// "\n");
			}
		} else {
			// a.log("  Encrypted block: " +
			// HexUtil.byteArrayToHexString(fc.data) + "\n");
		}

		num = track2.substring(track2.indexOf(";") + 1, track2.indexOf("="));
		return num;

	}
}
