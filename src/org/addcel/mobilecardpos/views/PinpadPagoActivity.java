package org.addcel.mobilecardpos.views;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.addcel.client.WebServiceClient;
import org.addcel.crypto.AddcelCrypto;
import org.addcel.datecs.pinpad.DeviceActivity;
import org.addcel.datecs.pinpad.PinpadHelper;
import org.addcel.datecs.pinpad.PinpadManager;
import org.addcel.datecs.pinpad.PinpadManager.OnDisconnectListener;
import org.addcel.datecs.pinpad.tlv.BerTlv;
import org.addcel.datecs.pinpad.tlv.Tag;
import org.addcel.datecs.pinpad.utils.CryptoUtil;
import org.addcel.datecs.pinpad.utils.HexUtil;
import org.addcel.interfaces.WSResponseListener;
import org.addcel.mobilcardpos.util.Constructores;
import org.addcel.mobilcardpos.util.Urls;
import org.addcel.mobilecardpos.R;
import org.addcel.mobilecardpos.session.SessionManager;
import org.addcel.util.AppUtils;
import org.addcel.util.DeviceUtils;
import org.json.JSONException;
import org.json.JSONObject;

import com.datecs.pinpad.DeviceInfo;
import com.datecs.pinpad.Pinpad;
import com.datecs.pinpad.PinpadException;
import com.datecs.pinpad.emv.EMVApplication;
import com.datecs.pinpad.emv.EMVCommonApplications;
import com.datecs.pinpad.emv.EMVStatusCodes;
import com.datecs.pinpad.emv.EMVTags;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.DialogInterface.OnKeyListener;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.Spannable;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class PinpadPagoActivity extends Activity {
	public static final String LOG_TAG = "AddcelPOSPinpad";
	private Context contxt = PinpadPagoActivity.this;
	private SessionManager session;
	String holder;
	String number;
	String year;
	String month;
	Button pagar;
	Button leerT;
	//EditText editProd;
	EditText editNombre;
	EditText editMail;
	EditText editYear;
	EditText editMonth;
	EditText editTipo;
	EditText editNum;
	EditText editTotal;
	EditText editCVV2;

	String monto;
	String nombre;
	String correo;
	//String descrip;
	String vigencia;
	String cvv2;
	// private String tarjetaNo;
	String mes;
	String anio;
	String tarjetaTipo;
	String tipoTarjeta;

	private static final int REQUEST_ENABLE_BT = 1;
	private static final int REQUEST_DEVICE = 2;

	private interface MethodInvoker {
		public void invoke(Pinpad pinpad) throws PinpadException, IOException,
				InterruptedException;
	}

	private TextView mLogView;
	private ScrollView mLogScrollView;
	private BluetoothAdapter mBtAdapter;
	private PinpadManager mPinpadManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.primapro_pago_activity);

		mLogScrollView = (ScrollView) findViewById(R.id.scroll_log);
		mLogScrollView.setHorizontalScrollBarEnabled(true);
		mLogView = (TextView) findViewById(R.id.txt_log);
		//mLogScrollView.setVisibility(View.GONE);
		
		List<String> list = new ArrayList<String>();
		list.add("CREDITO");
		list.add("DEBITO");
		Spinner spinner2 = (Spinner) findViewById(R.id.spinnerTipo);
		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, list);
		dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner2.setAdapter(dataAdapter);
		
		spinner2.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				// TODO Auto-generated method stub
				tipoTarjeta = parent.getItemAtPosition(pos).toString();	
				Toast.makeText(
						contxt,
						"Tipo de tarjeta: "
								+ tipoTarjeta,
						Toast.LENGTH_SHORT).show();			
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				// TODO Auto-generated method stub
				tipoTarjeta = parent.getItemAtPosition(0).toString();	
			}
		});
		
		TextView txtVersion = (TextView) findViewById(R.id.txt_version);
		txtVersion.setText(Build.MANUFACTURER + " " + Build.MODEL);
		session = new SessionManager(contxt);

		//editProd = (EditText) findViewById(R.id.productoPrima);
		editMail = (EditText) findViewById(R.id.correoPrima);
		editMonth = (EditText) findViewById(R.id.monthVigencia);
		editNombre = (EditText) findViewById(R.id.nombrePrima);
		editTipo = (EditText) findViewById(R.id.tipoTarjeta);
		editYear = (EditText) findViewById(R.id.yearVigencia);
		editNum = (EditText) findViewById(R.id.numeroTarjeta);
		editCVV2 = (EditText) findViewById(R.id.cvvTarjeta);
		editTotal = (EditText) findViewById(R.id.totalPrima);

		mPinpadManager = PinpadManager.getInstance(this);
		mPinpadManager.setOnDisconnectListener(new OnDisconnectListener() {

			@Override
			public void OnDisconnect() {
				// TODO Auto-generated method stub
				showToast("Conexi�n de Pinpad cerrada");
				waitForDevice();
			}
		});
		mBtAdapter = BluetoothAdapter.getDefaultAdapter();
		if (mBtAdapter != null) {
			if (mBtAdapter.isEnabled()) {
				waitForDevice();
			} else {
				enableBluetooth();
			}
		} else {
			Toast.makeText(this, "El dispositivo no soporta bluetooth",
					Toast.LENGTH_SHORT).show();
			startActivity(new Intent(PinpadPagoActivity.this,
					MenuInicioActivity.class));
			return;
		}
	
		Button readcard = (Button)findViewById(R.id.btn_read_card);
		readcard.setOnClickListener(
				new OnClickListener() {
					String ERROR_MSG = "El campo no puede est�r vac�o";
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						if (TextUtils.isEmpty(editTotal.getText())) {
							editTotal.setError(ERROR_MSG);
							Toast.makeText(contxt, "Campo Total vacio",
									Toast.LENGTH_SHORT).show();
						}else {
							
							editTotal.setKeyListener(null);
							editTotal.setFocusable(false);
							monto = editTotal.getText().toString().trim();
							startTransation();
						}
						
						
					}
				});
		
		Button pagar = (Button)findViewById(R.id.btn_pagar);
		pagar.setOnClickListener(new OnClickListener() {
			String ERROR_MSG = "El campo no puede est�r vac�o";
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
	
				if (TextUtils.isEmpty(editNombre.getText())) {
				//	editNombre.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Nombre vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editMail.getText())) {
				//	editMail.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Coreo vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editCVV2.getText())) {
				//	editCVV2.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo CVV2 vacio",
							Toast.LENGTH_SHORT).show();
				} else if (TextUtils.isEmpty(editTotal.getText())) {
				//	editTotal.setError(ERROR_MSG);
					Toast.makeText(contxt, "Campo Total vacio",
							Toast.LENGTH_SHORT).show();
				} else {
					//descrip = editProd.getText().toString().trim();
					nombre = editNombre.getText().toString().trim();
					correo = editMail.getText().toString().trim();
					cvv2 = editCVV2.getText().toString().trim();
					// tarjetaNo = editNum.getText().toString().trim();
					vigencia = mes + "/" + anio;
					monto = editTotal.getText().toString().trim();
					pedirToken();
				}
			}
		});
	}

	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		mPinpadManager.setOnDisconnectListener(null);
		mPinpadManager.release();
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		startActivity(new Intent(PinpadPagoActivity.this,
				MenuInicioActivity.class));
		finish();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		switch (requestCode) {
		case REQUEST_ENABLE_BT: {
			if (resultCode == RESULT_OK) {
				waitForDevice();
			} else {
				startActivity(new Intent(PinpadPagoActivity.this,
						MenuInicioActivity.class));
			}
			break;
		}
		case REQUEST_DEVICE: {
			if (resultCode == RESULT_OK) {
				clearLog();
				initPinpad();
			} else {
				startActivity(new Intent(PinpadPagoActivity.this,
						MenuInicioActivity.class));

			}
			break;
		}

		}
	}

	private void enableBluetooth() {
		Intent enableBtIntent = new Intent(
				BluetoothAdapter.ACTION_REQUEST_ENABLE);
		startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
	}

	private void waitForDevice() {
		Intent selectDevice = new Intent(this, DeviceActivity.class);
		startActivityForResult(selectDevice, REQUEST_DEVICE);
	}

	private void showToast(final String text) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Toast.makeText(getApplicationContext(), text,
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	private void clearLog() {
		mLogView.setText("");
	}

	private void postColoredText(final TextView textView,
			final ScrollView scrollView, final String text, final int color,
			final boolean bold) {
		Log.v(LOG_TAG, text);

		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				int start = textView.getText().length();
				textView.append(text);
				int end = textView.getText().length();
				Spannable spannableText = (Spannable) textView.getText();
				spannableText.setSpan(new ForegroundColorSpan(color), start,
						end, 0);
				if (bold) {
					spannableText.setSpan(new StyleSpan(
							android.graphics.Typeface.BOLD), start, end, 0);
				}

				if (scrollView != null) {
					scrollView.post(new Runnable() {
						@Override
						public void run() {
							scrollView.fullScroll(View.FOCUS_DOWN);
						}
					});
				}
			}
		});
		Log.i(LOG_TAG, text);
	}

	private void postLog(String text) {
		postLog(text, false);
	}

	private void postLog(String text, boolean bold) {
		postColoredText(mLogView, mLogScrollView, text, Color.BLACK, bold);
	}

	private void postError(final String text) {
		postColoredText(mLogView, mLogScrollView, text, Color.RED, true);
	}

	private void postWarning(final String text) {
		postColoredText(mLogView, mLogScrollView, text, 0xFFFF7F00, false);
	}

	private byte[] decode(String s) {
		return HexUtil.hexStringToByteArray(s, ' ');
	}

	private byte[] decryptData(byte[] data) {
		byte[] DATA_KEY = decode("B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 BA BB BC BD BE BF");
		byte[] decrypted = CryptoUtil.decrypt3DESCBC(DATA_KEY, new byte[8],
				data);
		if (decrypted.length < 12)
			return null;

		int length = (decrypted[8] & 0xff) + (decrypted[9] & 0xff);
		if (decrypted.length < (length + 12))
			return null;

		int origCrc = ((decrypted[10 + length] & 0xff) << 8)
				+ (decrypted[11 + length] & 0xff);
		int calcCrc = CryptoUtil.crcccitt16(0xFFFF, decrypted, 0, 10 + length);
		if (origCrc != calcCrc)
			return null;

		return Arrays.copyOfRange(decrypted, 10, 10 + length);
	}

	private String readTagAsString(Pinpad pinpad, int tag)
			throws PinpadException, IOException, InterruptedException {
		byte[] encData = pinpad.emvGetTags3DESCBC(3, 0xAABBCCDD,
				new Tag(tag).getBytes());
		byte[] tagData = decryptData(encData);
		if (tagData == null) {
			throw new InterruptedException("Failed to decrypt data");
		}
		return BerTlv.create(tagData).getValueAsHexString();
	}

	private static String emvStatusToString(int status) {
		switch (status) {
		case EMVStatusCodes.EMV_SUCCESS:
			return "Operation successful";
		case EMVStatusCodes.EMV_LIST_AVAILABLE:
			return "More than one matching applications found";
		case EMVStatusCodes.EMV_APPLICATION_AVAILABLE:
			return "Only one matching application found";
		case EMVStatusCodes.EMV_NO_COMMON_APPLICATION:
			return "No matching applications found";
		case EMVStatusCodes.EMV_EASY_ENTRY_APP:
			return "Easy Entry application";
		case EMVStatusCodes.EMV_AMOUNT_NEEDED:
			return "Amount is requested by the dynamic data authentication";
		case EMVStatusCodes.EMV_RESULT_NEEDED:
			return "Result needed";
		case EMVStatusCodes.EMV_AUTH_COMPLETED:
			return "Authentication is completed";
		case EMVStatusCodes.EMV_AUTH_NOT_DONE:
			return "Authentication was not performed";
		case EMVStatusCodes.EMV_OFFLINE_PIN_PLAIN:
			return "OFFLINE plain text pin is required";
		case EMVStatusCodes.EMV_ONLINE_PIN:
			return "ONLINE pin is required";
		case EMVStatusCodes.EMV_OFFLINE_PIN_CIPHERED:
			return "OFFLINE ciphered pin is required";
		case EMVStatusCodes.EMV_BLOCKED_APPLICATION:
			return "Explicit selection was done and blocked AIDs were found";
		case EMVStatusCodes.EMV_TRANSACTION_ONLINE:
			return "An online request should be done";
		case EMVStatusCodes.EMV_TRANSACTION_APPROVED:
			return "Transaction can be accepted offline";
		case EMVStatusCodes.EMV_TRANSACTION_DENIED:
			return "Transaction must be declined";
		case EMVStatusCodes.EMV_CDA_FAILED:
			return "CDA failed and the cryptogram got is not an AAC or the data handed for DDA was not found";
		case EMVStatusCodes.EMV_INVALID_PIN:
			return "Incorrect PIN";
		case EMVStatusCodes.EMV_INVALID_PIN_LAST_ATTEMPT:
			return "Incorrect PIN, last attempt available only";
		case EMVStatusCodes.EMV_FAILURE:
			return "Command failed, possibly due wrong input parameters - wrong ATR, bit values, etc";
		case EMVStatusCodes.EMV_NO_DATA_FOUND:
			return "Incoming data pointer is null or empty";
		case EMVStatusCodes.EMV_SYSTEM_ERROR:
			return "Internal system error occurred";
		case EMVStatusCodes.EMV_DATA_FORMAT_ERROR:
			return "Incorrect format found in the input parameters";
		case EMVStatusCodes.EMV_INVALID_ATR:
			return "Invalid ATR sequence, not according to specification";
		case EMVStatusCodes.EMV_ABORT_TRANSACTION:
			return "Severe error occurred transaction must be aborted";
		case EMVStatusCodes.EMV_APPLICATION_NOT_FOUND:
			return "AID not found in the card";
		case EMVStatusCodes.EMV_INVALID_APPLICATION:
			return "Application is not correct";
		case EMVStatusCodes.EMV_ERROR_IN_APPLICATION:
			return "Some error during read process";
		case EMVStatusCodes.EMV_CARD_BLOCKED:
			return "Status word got from the PSE selection indicates that the card is blocked";
		case EMVStatusCodes.EMV_NO_SCRIPT_LOADED:
			return "No script loaded";
		case EMVStatusCodes.EMV_TAG_NOT_FOUND:
			return "Tag not found";
		case EMVStatusCodes.EMV_INVALID_TAG:
			return "Tag cannot be read";
		case EMVStatusCodes.EMV_INVALID_LENGTH:
			return "Length of the buffer is incorrect";
		case EMVStatusCodes.EMV_INVALID_HASH:
			return "Error in the HASH verification";
		case EMVStatusCodes.EMV_INVALID_KEY:
			return "No key was found to do the verification";
		case EMVStatusCodes.EMV_NO_MORE_KEYS:
			return "No more available locations for keys";
		case EMVStatusCodes.EMV_ERROR_AC_PROCESS:
			return "Error processing the AC generation";
		case EMVStatusCodes.EMV_ERROR_AC_DENIED:
			return "Status word got from the card is 6985";
		case EMVStatusCodes.EMV_NO_CURRENT_METHOD:
			return "No method is currently applicable";
		case EMVStatusCodes.EMV_RESULT_ALREADY_LOADED:
			return "Result already loaded for the current method";
		case EMVStatusCodes.EMV_INVALID_REMAINDER:
			return "Invalid reminder";
		case EMVStatusCodes.EMV_INVALID_HEADER:
			return "Invalid header";
		case EMVStatusCodes.EMV_INVALID_FOOTER:
			return "Invalid footer";
		case EMVStatusCodes.EMV_INVALID_FORMAT:
			return "Invalid format";
		case EMVStatusCodes.EMV_INVALID_CERTIFICATE:
			return "Invalid certificate";
		case EMVStatusCodes.EMV_INVALID_SIGNATURE:
			return "Invalid signature";
		default:
			return "Unpecified status code " + status;
		}
	}

	private void throwOnEMVError(Pinpad pinpad) throws InterruptedException {
		int status = pinpad.emvGetLastStatus();
		if (status != EMVStatusCodes.EMV_SUCCESS) {
			throw new InterruptedException("EMV Error: "
					+ emvStatusToString(status));
		}
	}

	private void invokeHelper(final MethodInvoker invoker) {
		final Pinpad pinpad = mPinpadManager.getPinpad();

		final ProgressDialog dialog = new ProgressDialog(this);
		dialog.setCancelable(false);
		dialog.setCanceledOnTouchOutside(false);
		dialog.setMessage("Espere porfavor");
		dialog.setOnKeyListener(new OnKeyListener() {
			@Override
			public boolean onKey(DialogInterface dialog, int keyCode,
					KeyEvent event) {
				return true;
			}
		});
		dialog.show();

		final Thread t = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					invoker.invoke(pinpad);
				} catch (PinpadException e) { // Not critical exception
					e.printStackTrace();
					StringWriter sw = new StringWriter();
					e.printStackTrace(new PrintWriter(sw));
					String stacktrace = sw.toString();
					postError("\nPINPAD ERROR: " + stacktrace + "\n\n");

				} catch (InterruptedException e) { // Not critical exception
					e.printStackTrace();
					postWarning("\nAbort: " + e.getMessage() + "\n\n");
				} catch (final IOException e) { // Critical exception
					e.printStackTrace();
					mPinpadManager.release();
					showToast(e.getMessage());
					waitForDevice();
				} finally {
					dialog.dismiss();
				}
			}
		});
		t.start();
	}

	private void initPinpad() {
		postLog("*** Init Pinpad ***\n", true);

		invokeHelper(new MethodInvoker() {
			@Override
			public void invoke(Pinpad pinpad) throws IOException,
					PinpadException, InterruptedException {
				// Log Bluetooth settings
				postLog("� Connection established\n");
				postLog("  Bluetooth name: "
						+ mPinpadManager.getBluetoothName() + "\n");
				postLog("  Bluetooth addr: "
						+ mPinpadManager.getBluetoothAddress() + "\n");

				// Log device information
				postLog("� Get device information\n");
				DeviceInfo devInfo = pinpad.getIdentification();
				postLog("  Device serial: " + devInfo.getDeviceSerialNumber()
						+ "\n");
				postLog("  Device type: " + devInfo.getDeviceType() + "\n");
				postLog("  CPU serial: " + devInfo.getCPUSerialNumber() + "\n");
				postLog("  Application name: " + devInfo.getApplicationName()
						+ "\n");
				postLog("  Application version: "
						+ devInfo.getApplicationVersion() + "\n");
				postLog("  Key version: " + devInfo.getKeyVersion() + "\n");

				// Set current time.
				postLog("� Set time\n");
				pinpad.sysSetDate(Calendar.getInstance());

				// Set display
				postLog("� Init display\n");
				pinpad.uiOpenTextWindow(0, 0, 16, 4, Pinpad.FONT_8X16,
						Pinpad.CP_LATIN1);

				// Disable all events.
				postLog("� Disable events\n");
				pinpad.sysEnableEvents(0);

				postLog("� Pinpad is ready.\n\n");
			}
		});
	}

	private void processEMVTransaction(Pinpad pinpad) throws PinpadException,
			IOException, InterruptedException {
		boolean completed = false;
		int emvStatus = 0;

		postLog("� Process EMV card\n");

		// /////////////////////////////////////////////////////////////////////
		// APPLICATION SELECTION & INITIALIZATION
		// /////////////////////////////////////////////////////////////////////

		// Load terminal application list.
		final EMVApplication[] emvApps = new EMVApplication[27];
		emvApps[0] = new EMVApplication(decode("A0 00 00 00 03 10 10"),
				"VISA CR/DB", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[1] = new EMVApplication(decode("A0 00 00 00 03 20 10"),
				"VISA ELECTRON", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[2] = new EMVApplication(decode("A0 00 00 00 04 10 10"),
				"MASTERCARD CR/DB", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[3] = new EMVApplication(decode("A0 00 00 00 04 30 60"),
				"MAESTRO", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[4] = new EMVApplication(decode("A0 00 00 00 04 99 99"),
				"MASTERCARD", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[5] = new EMVApplication(decode("A0 00 00 00 03 40 10"), "VISA",
				EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[6] = new EMVApplication(decode("A0 00 00 00 03 50 10"), "VISA",
				EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[7] = new EMVApplication(decode("A0 00 00 00 04 20 10"),
				"MASTERCARD", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[8] = new EMVApplication(decode("A0 00 00 00 04 30 10"),
				"MASTERCARD", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[9] = new EMVApplication(decode("A0 00 00 00 04 40 10"),
				"MASTERCARD", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[10] = new EMVApplication(decode("A0 00 00 00 04 50 10"),
				"MASTERCARD", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[11] = new EMVApplication(decode("A0 00 00 00 03 20 20"),
				"V PAY", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[12] = new EMVApplication(decode("A0 00 00 00 03 80 10"),
				"VISA PLUS", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[13] = new EMVApplication(decode("A0 00 00 00 03 30 10"),
				"VISA INTERLINK", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[14] = new EMVApplication(decode("A0 00 00 00 04 60 00"),
				"CIRRUS", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[15] = new EMVApplication(decode("A0 00 00 00 25 01 00"),
				"AMERICAN EXPRESS", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[16] = new EMVApplication(decode("A0 00 00 00 25 00 00"),
				"AMERICAN EXPRESS", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[17] = new EMVApplication(decode("A0 00 00 00 65 10 10"), "JCB",
				EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[18] = new EMVApplication(decode("A0 00 00 00 10 30 30"),
				"UNKNOWN", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[19] = new EMVApplication(decode("A0 00 00 00 99 90 90"),
				"UNKNOWN", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[20] = new EMVApplication(decode("A0 00 00 00 05 00 01"),
				"MAESTRO UK", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[21] = new EMVApplication(decode("A0 00 00 00 05 00 02"),
				"UK SOLO", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[22] = new EMVApplication(decode("A0 00 00 01 52 30 10"),
				"DISCOVER", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[23] = new EMVApplication(decode("A0 00 00 00 29 10 10"),
				"LINK ATM NETWORK", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[24] = new EMVApplication(decode("A0 00 00 01 21 10 10"),
				"DANKORT", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[25] = new EMVApplication(decode("A0 00 00 01 41 00 01"),
				"CoGeBan", EMVApplication.PARTIAL_MATCH_VISA);
		emvApps[26] = new EMVApplication(decode("A0 00 00 00 42 10 10"),
				"CB CARD", EMVApplication.PARTIAL_MATCH_VISA);

		/*
		 * final EMVApplication[] emvApps = new EMVApplication[8]; emvApps[0] =
		 * new EMVApplication(decode("A0 00 00 00 03 10 10"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[1] = new
		 * EMVApplication(decode("A0 00 00 00 03 20 10"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[2] = new
		 * EMVApplication(decode("A0 00 00 00 04 10 10"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[3] = new
		 * EMVApplication(decode("A0 00 00 00 03 30 10"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[4] = new
		 * EMVApplication(decode("A0 00 00 00 04 60 00"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[5] = new
		 * EMVApplication(decode("A0 00 00 00 04 30 60"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[6] = new
		 * EMVApplication(decode("A0 00 00 00 10 30 30"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA); emvApps[7] = new
		 * EMVApplication(decode("A0 00 00 00 65 10 10"), "VISA/MASTER",
		 * EMVApplication.PARTIAL_MATCH_VISA);
		 */
		postLog("� Load application list\n");
		pinpad.emvLoadAppList(EMVApplication.SELECTION_METHOD_PSE, false,
				emvApps);
		throwOnEMVError(pinpad);

		do {
			EMVApplication emvApp = null;
			// Get common application list
			EMVCommonApplications emvCommonApps = pinpad.emvGetCommonAppList();

			// The EMV status of this operation must be EMV_LIST_AVAILABLE or
			// EMV_APPLICATION_AVAILABLE
			emvStatus = pinpad.emvGetLastStatus();

			switch (emvStatus) {
			case EMVStatusCodes.EMV_APPLICATION_AVAILABLE: {
				emvApp = emvCommonApps.getApplications()[0];

				if (emvCommonApps.isConfirmationRequired()) {
					if (PinpadHelper.confirm(pinpad, emvApp.getLabelString()
							+ "?")) {

					} else {
						throw new InterruptedException(
								"No application confirmed");
					}
				}
				break;
			}
			case EMVStatusCodes.EMV_LIST_AVAILABLE: {
				postLog("� Select application:\n");
				String[] items = new String[emvCommonApps.getApplications().length];
				for (int i = 0; i < emvCommonApps.getApplications().length; i++) {
					EMVApplication app = emvCommonApps.getApplications()[i];
					postLog("  -> " + app.getLabelString() + "\n");
					postLog("  " + "AID: "
							+ HexUtil.byteArrayToHexString(app.getAID()) + "\n");
					items[i] = app.getLabelString();
				}

				int index = PinpadHelper.menu(pinpad, items);
				emvApp = emvCommonApps.getApplications()[index];
				break;
			}
			case EMVStatusCodes.EMV_NO_COMMON_APPLICATION: {
				throw new InterruptedException("No common application found");
			}
			default: {
				throwOnEMVError(pinpad);
			}
			}

			if (emvApp == null) { // ?!?
				throw new InterruptedException("SYSTEM ERROR");
			}

			postLog("  Application: " + emvApp.getLabelString() + "\n");
			tarjetaTipo = emvApp.getLabelString();
			editTipo.post(new Runnable() {

				@Override
				public void run() {
					// TODO Auto-generated method stub
					editTipo.setText(tarjetaTipo);
				}
			});
			postLog("� Set EMV tags\n");
			pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_TIME, "115303");
			throwOnEMVError(pinpad);
			pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_DATE, "130514");
			throwOnEMVError(pinpad);
			pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_SEQ_COUNTER,
					"0001");
			throwOnEMVError(pinpad);
			pinpad.emvSetDataAsString(EMVTags.TAG_APP_VERSION_NUMBER, "0096");
			throwOnEMVError(pinpad);

			// Initial application processing.
			pinpad.emvInitialAppProcessing(emvApp.getAID());
			throwOnEMVError(pinpad);
			emvStatus = pinpad.emvGetLastStatus();
			switch (emvStatus) {
			case EMVStatusCodes.EMV_BLOCKED_APPLICATION: {
				throw new InterruptedException("Blocked application");
			}
			case EMVStatusCodes.EMV_APPLICATION_NOT_FOUND:
			case EMVStatusCodes.EMV_INVALID_APPLICATION: {
				continue;
			}
			default: {
				throwOnEMVError(pinpad);
			}
			}

			completed = true;
		} while (!completed);

		PinpadHelper.busyTransaction(pinpad);

		String tag;
		postLog("� Read EMV tags\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_LANGUAGE_PREFERENCE);
		throwOnEMVError(pinpad);
		postLog("  TAG_LANGUAGE_PREFERENCE: " + tag + "\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_ISSUER_CODE_INDEX);
		throwOnEMVError(pinpad);
		postLog("  TAG_ISSUER_CODE_INDEX: " + tag + "\n");

		postLog("� Read application data\n");
		pinpad.emvReadAppData(new int[] {});
		throwOnEMVError(pinpad);

		// /////////////////////////////////////////////////////////////////////
		// TRANSACTION DATA PROCESSING
		// /////////////////////////////////////////////////////////////////////
		postLog("� Read EMV tags\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_EFFECTIVE_DATE);
		throwOnEMVError(pinpad);
		postLog("  TAG_EFFECTIVE_DATE: " + tag + "\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_EXPIRY_DATE);
		throwOnEMVError(pinpad);
		postLog("  TAG_EXPIRY_DATE: " + tag + "\n");
		anio = tag.substring(0, 2);
		editYear.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editYear.setText(anio);
			}
		});
		mes = tag.substring(2, 4);
		editMonth.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editMonth.setText(mes);
			}
		});
		// Tags with sensitive data
		tag = readTagAsString(pinpad, EMVTags.TAG_PAN);
		throwOnEMVError(pinpad);
		postLog("  TAG_PAN: " + tag + "\n");
		number = tag;
		editNum.post(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				editNum.setText(number);
			}
		});

		tag = readTagAsString(pinpad, EMVTags.TAG_PAN_SEQUENCE_NUMBER);
		throwOnEMVError(pinpad);
		postLog("  TAG_PAN_SEQUENCE_NUMBER: " + tag + "\n");
		tag = readTagAsString(pinpad, EMVTags.TAG_TRACK2_EQUIVALENT_DATA);
		throwOnEMVError(pinpad);
		postLog("  TAG_TRACK2_EQUIVALENT_DATA: " + tag + "\n");

		tag = pinpad.emvGetDataAsString(EMVTags.TAG_ISSUER_COUNTRY_CODE);
		throwOnEMVError(pinpad);
		postLog("  TAG_ISSUER_COUNTRY_CODE: " + tag + "\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_APP_USAGE_CONTROL);
		throwOnEMVError(pinpad);
		postLog("  TAG_APP_USAGE_CONTROL: " + tag + "\n");

		postLog("� Set EMV tags\n");
		pinpad.emvSetDataAsString(EMVTags.TAG_TERM_ACTION_ONLINE, "DC4004F800");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TERM_ACTION_DENIAL, "0010000000");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TERM_ACTION_DEFAULT, "DC4000A800");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_DEFAULT_DDOL, "9F3704");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_DEFAULT_TDOL, "9F3704");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_TYPE, "01");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_AMOUNT_AUTHORISED_NUM,
				"000000002000");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_AMOUNT_AUTHORISED_BINARY,
				"000007D0");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_CURR_CODE, "0978");
		throwOnEMVError(pinpad);
		// pinpad.emvSetDataAsString(EMVTags.TAG_ACCOUNT_TYPE, "???");
		// throwOnEMVError(pinpad);

		// postLog("Set bypass mode\n");
		// pinpad.emvSetBypassMode(Pinpad.EMV_BYPASS_CURRENT_METHOD_MODE);
		// throwOnEMVError(pinpad);

		postLog("� Authentication\n");
		postLog(" No se hace autenticacion\n");
		//pinpad.emvAuthentication(false);
		//throwOnEMVError(pinpad);

		postLog("� Process restrictions\n");
		pinpad.emvProcessRestrictions();
		throwOnEMVError(pinpad);

		postLog("� Set EMV tags\n");
		pinpad.emvSetDataAsString(EMVTags.TAG_RISK_AMOUNT, "00000000");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_FLOOR_LIMIT_CURRENCY, "0826");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TERMINAL_FLOOR_LIMIT, "00000000");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_THRESHOLD_VALUE, "00000000");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_TARGET_PERCENTAGE, "20");
		throwOnEMVError(pinpad);
		pinpad.emvSetDataAsString(EMVTags.TAG_MAX_TARGET_PERCENTAGE, "80");
		throwOnEMVError(pinpad);

		postLog("� Terminal risk\n");
		pinpad.emvTerminalRisk(false);
		throwOnEMVError(pinpad);

		tag = pinpad.emvGetDataAsString(EMVTags.TAG_TVR);
		throwOnEMVError(pinpad);
		postLog("  TAG_TVR: " + tag + "\n");

		if (tipoTarjeta == "DEBITO") {
			completed = false;
			do {
				postLog("� Get authentication method\n");
				pinpad.emvGetAuthenticationMethod();
				emvStatus = pinpad.emvGetLastStatus();

				if (emvStatus == EMVStatusCodes.EMV_AUTH_COMPLETED) {
					postLog("  EMV Status: AUTH COMPLETED\n");
					completed = true;
				} else {
					int authResult = -1;

					if (emvStatus == EMVStatusCodes.EMV_ONLINE_PIN) {
						postLog("  EMV Status: ONLINE PIN\n");

						postLog("� Enter PIN\n");
						try {
							pinpad.uiEnterPin(1, 2, 20, '*',
									"TOTAL: $" + monto + "\nINTRODUCIR PIN:\n");
						} catch (PinpadException e) {
							int err = e.getErrorCode();

							if (err == Pinpad.ERROR_CANCEL
									|| err == Pinpad.ERROR_TIMEOUT) {
								throw new InterruptedException("PIN canceled");
							}

							throw e;
						}

						PinpadHelper.busyTransaction(pinpad);

						// Generate new DUKPT key, which stay active up to 3 minutes
						// and will be erased after method uiVerifyPINOnline is
						// called.
						byte[] ksn = pinpad.dukptGenerateKeyOnline(0);
						postLog("  DUKPT KSN: " + HexUtil.byteArrayToHexString(ksn)
								+ "\n");

						byte pinBlock[] = pinpad.uiVerifyPINOnline(Pinpad.DUKPT,
								Pinpad.ISO1, "0000000000000000".getBytes() /*
																			 * variant
																			 * must
																			 * be 16
																			 * bytes
																			 * long
																			 */,
								0x00, null /* pan is not used with ISO1 */);
						postLog("  ENCRTYPTED PIN: "
								+ HexUtil.byteArrayToHexString(pinBlock) + "\n");

						// Here you need to perform online PIN processing
						// ...

						authResult = Pinpad.EMV_AUTH_RESULT_SUCCESS;
					} else if (emvStatus == EMVStatusCodes.EMV_OFFLINE_PIN_CIPHERED
							|| emvStatus == EMVStatusCodes.EMV_OFFLINE_PIN_PLAIN) {
						postLog("  EMV Status: OFFLINE PIN (CIPHERED/PLAIN)\n");

						try {
							postLog("� Enter PIN");
							pinpad.uiEnterPin(1, 2, 20, '*',
									"AMOUNT: $1.99\nEnter PIN:\n");
						} catch (PinpadException e) {
							int err = e.getErrorCode();

							if (err == Pinpad.ERROR_CANCEL
									|| err == Pinpad.ERROR_TIMEOUT) {
								throw new InterruptedException("PIN canceled");
							} else if (err == Pinpad.ERROR_NO_DATA) {
								// If the empty pin entry is permit
								authResult = Pinpad.EMV_AUTH_FAIL_PIN_ENTRY_NOT_DONE;
							} else {
								authResult = Pinpad.EMV_AUTH_RESULT_FAILURE;
							}
						}

						if (authResult == -1) {
							postLog("� Verify PIN offline");
							pinpad.emvVerifyPinOffline();
							emvStatus = pinpad.emvGetLastStatus();
							if (emvStatus == EMVStatusCodes.EMV_SUCCESS) {
								authResult = Pinpad.EMV_AUTH_RESULT_SUCCESS;
							} else {
								authResult = Pinpad.EMV_AUTH_RESULT_FAILURE;
							}
						}
					}

					postLog("� Set authentication result: " + authResult + "\n");
					pinpad.emvSetAuthenticationResult(authResult);
					throwOnEMVError(pinpad);
				}
			} while (!completed);
	
		}
		
		
		postLog("� Read EMV tag\n");
		tag = pinpad.emvGetDataAsString(EMVTags.TAG_CH_VERIF_METHOD_RESULT);
		throwOnEMVError(pinpad);
		postLog("  TAG_CH_VERIF_METHOD_RESULT: " + tag + "\n");

		tag = pinpad.emvGetDataAsString(EMVTags.TAG_TVR);
		throwOnEMVError(pinpad);
		postLog("  TAG_TVR: " + tag + "\n");

		// /////////////////////////////////////////////////////////////////////
		// APPLICATION TRANSACTION DECISION
		// /////////////////////////////////////////////////////////////////////
		// If card is into hot list
		// pinpad.emvUpdateTVR(...); throwOnEMVError(pinpad);
		postLog("� Make transaction decision\n");
		pinpad.emvMakeTransactionDecision();
		emvStatus = pinpad.emvGetLastStatus();

		switch (emvStatus) {
		case EMVStatusCodes.EMV_TRANSACTION_APPROVED: {
			postLog("  EMV Status: TRANSACTION APPROVED\n");
			// ????????
			postLog("� Authenticate Issuer\n");
			pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_CATEGORY_CODE,
					"52");
			throwOnEMVError(pinpad);
			pinpad.emvSetDataAsString(EMVTags.TAG_ISSUER_AUTH_DAT,
					"FDD6818EE93367463030");
			throwOnEMVError(pinpad);
			pinpad.emvAuthenticateIssuer();
			throwOnEMVError(pinpad);
			break;
		}
		case EMVStatusCodes.EMV_TRANSACTION_DENIED: {
			postWarning("  EMV Status: TRANSACTION DENIED\n");
			throwOnEMVError(pinpad);
			break;
		}
		case EMVStatusCodes.EMV_TRANSACTION_ONLINE: {
			postLog("  EMV Status: TRANSACTION ONLINE\n");

			postLog("� Authenticate Issuer\n");
			pinpad.emvSetDataAsString(EMVTags.TAG_TRANSACTION_CATEGORY_CODE,
					"52");
			throwOnEMVError(pinpad);
			pinpad.emvSetDataAsString(EMVTags.TAG_ISSUER_AUTH_DAT,
					"FDD6818EE93367463030");
			throwOnEMVError(pinpad);
			pinpad.emvAuthenticateIssuer();
			throwOnEMVError(pinpad);
			break;
		}
		default: {
			throwOnEMVError(pinpad);
		}
		}

		PinpadHelper.successTransaction(pinpad);
		postLog("� Transaction completed.\n\n");
	}

	private void processMagneticCardTransaction(Pinpad pinpad, byte[] cardData)
			throws PinpadException, IOException, InterruptedException {
		postLog("� Process magnetic card\n");
		StringBuffer[] tracks = new StringBuffer[] { new StringBuffer(),
				new StringBuffer(), new StringBuffer(), };

		for (int i = 0, j = -1; i < cardData.length; i++) {
			int value = cardData[i] & 0xff;

			switch (value) {
			case 0xF1:
				j = 0;
				continue;
			case 0xF2:
				j = 1;
				continue;
			case 0xF3:
				j = 2;
				continue;
			default: {
				if (j > -1) {
					tracks[j].append((char) value);
				}
			}
			}
		}

		postLog("  (1)Track data: " + tracks[0].toString() + "\n");
		postLog("  (2)Track data: " + tracks[1].toString() + "\n");
		postLog("  (3)Track data: " + tracks[2].toString() + "\n");
		PinpadHelper.busyTransaction(pinpad);

		postLog("� Enter PIN\n");
		try {
			pinpad.uiEnterPin(1, 2, 20, '*', "AMOUNT: $1.99\nEnter PIN:\n");
		} catch (PinpadException e) {
			int err = e.getErrorCode();

			if (err == Pinpad.ERROR_CANCEL || err == Pinpad.ERROR_TIMEOUT) {
				throw new InterruptedException("PIN canceled");
			}

			throw e;
		}

		PinpadHelper.busyTransaction(pinpad);

		// Generate new DUKPT key, which stay active up to 3 minutes and will be
		// erased after method uiVerifyPINOnline is called.
		byte[] ksn = pinpad.dukptGenerateKeyOnline(0);
		postLog("  DUKPT KSN: " + HexUtil.byteArrayToHexString(ksn) + "\n");

		byte pinBlock[] = pinpad.uiVerifyPINOnline(Pinpad.DUKPT, Pinpad.ISO1,
				"0000000000000000".getBytes() /*
											 * variant must be 16 bytes long
											 */, 0x00, null /*
															 * pan is not used
															 * with ISO1
															 */);
		postLog("  ENCRTYPTED PIN: " + HexUtil.byteArrayToHexString(pinBlock)
				+ "\n");

		PinpadHelper.successTransaction(pinpad);
	}

	private void processManualEntryTransaction(Pinpad pinpad)
			throws PinpadException, IOException, InterruptedException {
		String pan = PinpadHelper.enterPAN(pinpad, 60000);
		postLog("  Card Number: " + pan + "\n");

		String exp = PinpadHelper.enterExpiryDate(pinpad, 60000);
		postLog("  Card Exp: " + exp + "\n");
	}

	private void startTransation() {
		postLog("*** Start transaction ***\n", true);
		
		loadKeys();

		invokeHelper(new MethodInvoker() {
			@Override
			public void invoke(final Pinpad pinpad) throws IOException,
					InterruptedException {
				try {
					boolean completed = false;

					// Init EMV kernel
					postLog("� Init EMV kernel\n");
					pinpad.emvDeinitialise();
					pinpad.emvInit();

					// Set EMV tags.
					postLog("� Set EMV tags\n");
					
					pinpad.emvSetDataAsString(
							EMVTags.TAG_ADD_TERM_CAPABILITIES, "6000803000");
					pinpad.emvSetDataAsString(EMVTags.TAG_SERIAL_NUMBER,
							"12345678");
					pinpad.emvSetDataAsString(
							EMVTags.TAG_TERMINAL_COUNTRY_CODE, "0826");
					pinpad.emvSetDataAsString(EMVTags.TAG_TERMINAL_TYPE, "22");
					throwOnEMVError(pinpad);
					pinpad.emvSetDataAsString(
							EMVTags.TAG_TERMINAL_CAPABILITIES, "E040C0");
					throwOnEMVError(pinpad);

					// Enable smart card reader.
					postLog("� Enable smart card reader\n");
					pinpad.scInit(Pinpad.SC_SLOT_MAIN, Pinpad.SC_VOLTAGE_5,
							Pinpad.SC_PROTOCOL_T0);

					while (!completed) {
						// Wait to insert card into Pinpad
						postLog("� Wait to insert card\n");
						boolean cardAvailable = PinpadHelper.insertCard(pinpad,
								60000);

						if (!cardAvailable) {
							postLog("� Enter PAN manually\n");
							processManualEntryTransaction(pinpad);
							completed = true;
							continue;
						}

						pinpad.scCardPowerOff(Pinpad.SC_SLOT_MAIN);

						// Check for smart card
						byte[] atr = null;
						try {
							atr = pinpad.scCardPowerOn(Pinpad.SC_SLOT_MAIN);
							postLog("� Smart card detected\n");
							postLog("  ATR: "
									+ HexUtil.byteArrayToHexString(atr) + "\n");
						} catch (PinpadException e) {
							postWarning("  No smart card detected\n");
						}

						boolean useMagneticData = false;

						if (atr != null) {
							postLog("� ATR Validation\n");
							pinpad.emvATRValidation(false, atr);
							if (pinpad.emvGetLastStatus() != EMVStatusCodes.EMV_SUCCESS) {
								postWarning("  Not a valid EMV card\n");
								atr = null;
							}

							processEMVTransaction(pinpad);
							completed = true;
						} else {
							useMagneticData = true;
						}

						pinpad.scCardPowerOff(Pinpad.SC_SLOT_MAIN);

						// Wait to remove card from Pinpad
						postLog("� Wait for remove card\n");
						byte[] cardData = PinpadHelper.removeCard(pinpad,
								useMagneticData);

						// If not EMV transaction is started then try to process
						// magnetic card transaction
						if (useMagneticData) {
							if (cardData != null) {
								byte[] encrypted = Arrays.copyOfRange(cardData,
										1, cardData.length);
								byte[] tracks = decryptData(encrypted);
								if (tracks == null) {
									throw new InterruptedException(
											"Failed to decrypt data");
								}
								processMagneticCardTransaction(pinpad, tracks);
								completed = true;
							} else {
								// Repeat transaction.
								postWarning("  Try again to read card\n");
								continue;
							}
						}
					}

					PinpadHelper.successTransaction(pinpad);
					postLog("� Transaction is successful.\n\n");
				} catch (InterruptedException e) {
					PinpadHelper.abortTransaction(pinpad);
					throw e;
				} catch (PinpadException e) {
					PinpadHelper.abortTransaction(pinpad);
					throw e;
				}
			}
		});
	}

	private void pedirToken() {
		JSONObject json = new JSONObject();

		try {
			json.put(Constructores.USER, "userPrueba");
			json.put(Constructores.PASSWORD, "passwordPrueba");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		new WebServiceClient(new TokenListener(), contxt, true,
				Urls.url_getToken).execute(AddcelCrypto.encryptHard(json
				.toString()));

	}

	private String token;

	private class TokenListener implements WSResponseListener {
		String TAG = "TokenListenerBluePad";

		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				try {
					JSONObject json = new JSONObject(response);
					token = json.getString(Constructores.TOKEN);
				} catch (JSONException e) {
					// TODO: handle exception
				}

				if (token != null) {
					Toast.makeText(contxt, "Token Exitoso", Toast.LENGTH_SHORT)
							.show();
					enviarPago();
				}
			}
		}

	}

	private void enviarPago() {
		JSONObject json = new JSONObject();
		String TAG = "PagoBluePad";

		try {
			json.put(Constructores.TOKEN, token);
			json.put(Constructores.CORREO, correo);
			json.put(Constructores.NOMBRE, nombre);
			json.put(Constructores.TARJETA, number);
			//json.put(Constructores.TARJETA,"1234567890123456");
			json.put(Constructores.CVV2, cvv2);
			json.put(Constructores.VIGENCIA, vigencia);
			//json.put(Constructores.VIGENCIA, "44/23");
			json.put(Constructores.PRODUCTO, "producto");
			json.put(Constructores.MONTO, monto);
			json.put(Constructores.IMEI, DeviceUtils.getIMEI(contxt));
			json.put(Constructores.TIPO_DISP, DeviceUtils.getTipo());
			json.put(Constructores.SOFTWARE, DeviceUtils.getSWVersion());
			json.put(Constructores.MODELO, DeviceUtils.getModel());
			//json.put(Constructores.LATITUD, session.getLatitud().toString());
			json.put(Constructores.LATITUD,"19.5337177");
			//json.put(Constructores.LONGITUD, session.getLongitud().toString());
			json.put(Constructores.LONGITUD,"-99.118539");
			json.put(Constructores.KEY, DeviceUtils.getIMEI(contxt));

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Log.i(TAG, json.toString());
		new WebServiceClient(new PagoListener(), contxt, true, Urls.url_pago)
				.execute(AddcelCrypto.encryptSensitive(AppUtils.getKey(),
						json.toString()));
	}

	private class PagoListener implements WSResponseListener {
		private static final String TAG = "PagoListener";
		private String errorMsj;
		private int resultado;
		// private String addcelIR;
		private String mont;
		private String autoriz;

		
		@Override
		public void StringResponse(String response) {
			// TODO Auto-generated method stub
			Log.i(TAG, "response: " + response);
			if (null == response || "".equals(response)) {
				Toast.makeText(contxt,
						"Error de conexi�n. Intente de nuevo m�s tarde.",
						Toast.LENGTH_SHORT).show();
			} else {
				response = AddcelCrypto.decryptHard(response);
				Log.i(TAG, response);

				JSONObject json = null;
				try {
					json = new JSONObject(response);
					resultado = json.getInt(Constructores.ERROR_ID);
					errorMsj = json.getString(Constructores.ERROR_MSG);
					// addcelIR = json.getString(Constructores.ADDCEL_IR);

				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				AlertDialog.Builder builder = new AlertDialog.Builder(contxt, 3);

				if (resultado == 1) {

					try {
						autoriz = json.getString(Constructores.AUTORIZACION);
						mont = json.getString(Constructores.MONTO);
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					builder.setTitle("Pago exitoso");
					builder.setMessage(errorMsj + "\nAutorizaci�n: " + autoriz
							+ "\nTotal: $ " + mont);

				} else {

					builder.setTitle("Error");
					builder.setMessage(errorMsj);

				}
				builder.setPositiveButton("ACEPTAR",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								// TODO Auto-generated method stub
								onDestroy();
								startActivity(new Intent(contxt,
										MenuInicioActivity.class));
							}
						});
				AlertDialog aldialog = builder.create();
				aldialog.show();
			}
		}

	}

	private void loadKeys(){
		postLog("*** Load keys ***\n", true);
		
		invokeHelper(new MethodInvoker() {
			
			@Override
			public void invoke(Pinpad pinpad) throws PinpadException, IOException,
					InterruptedException {
				// TODO Auto-generated method stub
				  // HMK - Hardware Manufacturer Key
			    byte[] HMK = decode("1A C4 F2 34 79 CD 8F 23 0B C4 9D 2C 98 C8 91 EA");  
			            
			    // Load KEK for DUKPT
			    byte[] KEK_DUKPT = decode("00 01 02 03 04 05 06 07 08 09 0A 0B 0C 0D 0E 0F");			    
			    postLog("� Load DUKPT KEK key\n");
			    pinpad.cryptoDeleteKey(1);
			    pinpad.cryptoExchangeECBKey(0, 1, Pinpad.KEY_KEK_DUKPT, 0, CryptoUtil.encrypt3DESECB(HMK, KEK_DUKPT));
			    
			    // Load KEK for data
                byte[] KEK_DATA = decode("10 11 12 13 14 15 16 17 18 19 1A 1B 1C 1D 1E 1F");               
                postLog("� Load data KEK key\n");
                pinpad.cryptoDeleteKey(2);
                pinpad.cryptoExchangeECBKey(0, 2, Pinpad.KEY_KEK_DATA_ENC, 0, CryptoUtil.encrypt3DESECB(HMK, KEK_DATA));
                
				// Load session key	
                postLog("� Load DUKPT key\n");
				byte[] KEY_IPEK = decode("A0 A1 A2 A3 A4 A5 A6 A7 A8 A9 AA AB AC AD AE AF");
				pinpad.cryptoExchangeECBKey(1, 0, Pinpad.KEY_DUKPT, 0x00, CryptoUtil.encrypt3DESECB(KEK_DUKPT, KEY_IPEK));
				
				// Load keys, used to encrypt the data.                 
				postLog("� Load data key\n");
                byte[] KEY_DATA = decode("B0 B1 B2 B3 B4 B5 B6 B7 B8 B9 BA BB BC BD BE BF");
                pinpad.cryptoDeleteKey(3); 
                pinpad.cryptoExchangeECBKey(2, 3, Pinpad.KEY_DATA_ENC, 0x00, CryptoUtil.encrypt3DESECB(KEK_DATA, KEY_DATA));
                
                // Save keys to flash
                postLog("� Save keys to NVRAM\n");
                pinpad.cryptoSaveKeysToFlash();
                
				// Load CA keys.		
				postLog("� Load CA keys\n");
				pinpad.caImportKey(0x01, decode("A0 00 00 00 03 01"), decode("03"), decode("C6 96 03 42 13 D7 D8 54 69 84 57 9D 1D 0F 0E A5 19 CF F8 DE FF C4 29 35 4C F3 A8 71 A6 F7 18 3F 12 28 DA 5C 74 70 C0 55 38 71 00 CB 93 5A 71 2C 4E 28 64 DF 5D 64 BA 93 FE 7E 63 E7 1F 25 B1 E5 F5 29 85 75 EB E1 C6 3A A6 17 70 69 17 91 1D C2 A7 5A C2 8B 25 1C 7E F4 0F 23 65 91 24 90 B9 39 BC A2 12 4A 30 A2 8F 54 40 2C 34 AE CA 33 1A B6 7E 1E 79 B2 85 DD 57 71 B5 D9 FF 79 EA 63 0B 75"));
				pinpad.caImportKey(0x02, decode("A0 00 00 00 03 07"), decode("03"), decode("A8 9F 25 A5 6F A6 DA 25 8C 8C A8 B4 04 27 D9 27 B4 A1 EB 4D 7E A3 26 BB B1 2F 97 DE D7 0A E5 E4 48 0F C9 C5 E8 A9 72 17 71 10 A1 CC 31 8D 06 D2 F8 F5 C4 84 4A C5 FA 79 A4 DC 47 0B B1 1E D6 35 69 9C 17 08 1B 90 F1 B9 84 F1 2E 92 C1 C5 29 27 6D 8A F8 EC 7F 28 49 20 97 D8 CD 5B EC EA 16 FE 40 88 F6 CF AB 4A 1B 42 32 8A 1B 99 6F 92 78 B0 B7 E3 31 1C A5 EF 85 6C 2F 88 84 74 B8 36 12 A8 2E 4E 00 D0 CD 40 69 A6 78 31 40 43 3D 50 72 5F")); 
				pinpad.caImportKey(0x03, decode("A0 00 00 00 03 08"), decode("03"), decode("D9 FD 6E D7 5D 51 D0 E3 06 64 BD 15 70 23 EA A1 FF A8 71 E4 DA 65 67 2B 86 3D 25 5E 81 E1 37 A5 1D E4 F7 2B CC 9E 44 AC E1 21 27 F8 7E 26 3D 3A F9 DD 9C F3 5C A4 A7 B0 1E 90 70 00 BA 85 D2 49 54 C2 FC A3 07 48 25 DD D4 C0 C8 F1 86 CB 02 0F 68 3E 02 F2 DE AD 39 69 13 3F 06 F7 84 51 66 AC EB 57 CA 0F C2 60 34 45 46 98 11 D2 93 BF EF BA FA B5 76 31 B3 DD 91 E7 96 BF 85 0A 25 01 2F 1A E3 8F 05 AA 5C 4D 6D 03 B1 DC 2E 56 86 12 78 59 38 BB C9 B3 CD 3A 91 0C 1D A5 5A 5A 92 18 AC E0 F7 A2 12 87 75 26 82 F1 58 32 A6 78 D6 E1 ED 0B")); 
				pinpad.caImportKey(0x04, decode("A0 00 00 00 03 09"), decode("03"), decode("9D 91 22 48 DE 0A 4E 39 C1 A7 DD E3 F6 D2 58 89 92 C1 A4 09 5A FB D1 82 4D 1B A7 48 47 F2 BC 49 26 D2 EF D9 04 B4 B5 49 54 CD 18 9A 54 C5 D1 17 96 54 F8 F9 B0 D2 AB 5F 03 57 EB 64 2F ED A9 5D 39 12 C6 57 69 45 FA B8 97 E7 06 2C AA 44 A4 AA 06 B8 FE 6E 3D BA 18 AF 6A E3 73 8E 30 42 9E E9 BE 03 42 7C 9D 64 F6 95 FA 8C AB 4B FE 37 68 53 EA 34 AD 1D 76 BF CA D1 59 08 C0 77 FF E6 DC 55 21 EC EF 5D 27 8A 96 E2 6F 57 35 9F FA ED A1 94 34 B9 37 F1 AD 99 9D C5 C4 1E B1 19 35 B4 4C 18 10 0E 85 7F 43 1A 4A 5A 6B B6 51 14 F1 74 C2 D7 B5 9F DF 23 7D 6B B1 DD 09 16 E6 44 D7 09 DE D5 64 81 47 7C 75 D9 5C DD 68 25 46 15 F7 74 0E C0 7F 33 0A C5 D6 7B CD 75 BF 23 D2 8A 14 08 26 C0 26 DB DE 97 1A 37 CD 3E F9 B8 DF 64 4A C3 85 01 05 01 EF C6 50 9D 7A 41")); 
				pinpad.caImportKey(0x05, decode("A0 00 00 00 04 00"), decode("03"), decode("9E 15 21 42 12 F6 30 8A CA 78 B8 0B D9 86 AC 28 75 16 84 6C 8D 54 8A 9E D0 A4 2E 7D 99 7C 90 2C 3E 12 2D 1B 9D C3 09 95 F4 E2 5C 75 DD 7E E0 A0 CE 29 3B 8C C0 2B 97 72 78 EF 25 6D 76 11 94 92 47 64 94 2F E7 14 FA 02 E4 D5 7F 28 2B A3 B2 B6 2C 9E 38 EF 65 17 82 3F 2C A8 31 BD DF 6D 36 3D"));  
				pinpad.caImportKey(0x06, decode("A0 00 00 00 04 98"), decode("03"), decode("A5 3E 6D D8 E6 7A EF 85 C9 91 82 EF 76 85 15 7D B0 11 14 B4 80 9B 15 2F 93 E5 59 CB 19 8E 86 C0 17 3C 0C 9B F1 65 17 B0 BE 21 A2 C0 9B 72 1F 78 93 F2 53 70 1B 87 AA 74 24 A0 49 5E E4 D7 C9 FA 9C 84 3E 80 9D 8A 51 49 10 D5 A7 26 9B A0 43 8B 85 8C 38 A6 89 15 06 78 AC 87 27 60 19 21 F8 4B"));
				pinpad.caImportKey(0x07, decode("A0 00 00 00 04 F1"), decode("03"), decode("A0 DC F4 BD E1 9C 35 46 B4 B6 F0 41 4D 17 4D DE 4A AB BB 82 8C 5A 83 4D 73 AA E2 7C 99 B0 B0 53 02 78 00 72 39 B6 45 9F F0 BB CD 7B 4B 9C 6C 50 AC CE 91 36 8D A1 BD 21 AA EA DB C6 53 47 33 7D 89 8F 5C 99 A0 9D 05 BE 02 DD 1F 8C 5B A2 0E 2F 13 2A 27 C4 1D 3F 85 CA D5 CF 66 68 E7 58 51 EC 66 EB F9 88 51 FD 4E 42 C4 4C 1D 59 F5 98 47 03 B2 7D 5F 21 B8 FA 0D 93 27 9F BB F6 9E 09 06 42 90 9C 9E AF 89 89 59 54 1A A6 75 7F 5F 62 41 04 F6 E1 D3 A9 53 2A 6E 51 51 5A EA D1 B4 3B 3D 78 35 08 8A 2F AF AE"));  
				pinpad.caImportKey(0x08, decode("A0 00 00 00 04 F3"), decode("03"), decode("98 F0 C7 70 F2 38 64 C2 E7 66 DF 02 D1 E8 33 DF F4 FF E9 2D 69 6E 16 42 F0 A8 8C 56 94 C6 47 9D 16 DB 15 37 BF E2 9E 4F DC 6E 6E 8A FD 1B 0E B7 EA 01 24 72 3C 33 31 79 BF 19 E9 3F 10 65 8B 2F 77 6E 82 9E 87 DA ED A9 C9 4A 8B 33 82 19 9A 35 0C 07 79 77 C9 7A FF 08 FD 11 31 0A C9 50 A7 2C 3C A5 00 2E F5 13 FC CC 28 6E 64 6E 3C 53 87 53 5D 50 95 14 B3 B3 26 E1 23 4F 9C B4 8C 36 DD D4 4B 41 6D 23 65 40 34 A6 6F 40 3B A5 11 C5 EF A3"));
				pinpad.caImportKey(0x09, decode("A0 00 00 00 04 F8"), decode("03"), decode("A1 F5 E1 C9 BD 86 50 BD 43 AB 6E E5 6B 89 1E F7 45 9C 0A 24 FA 84 F9 12 7D 1A 6C 79 D4 93 0F 6D B1 85 2E 25 10 F1 8B 61 CD 35 4D B8 3A 35 6B D1 90 B8 8A B8 DF 04 28 4D 02 A4 20 4A 7B 6C B7 C5 55 19 77 A9 B3 63 79 CA 3D E1 A0 8E 69 F3 01 C9 5C C1 C2 05 06 95 92 75 F4 17 23 DD 5D 29 25 29 05 79 E5 A9 5B 0D F6 32 3F C8 E9 27 3D 6F 84 91 98 C4 99 62 09 16 6D 9B FC 97 3C 36 1C C8 26 E1"));
				pinpad.caImportKey(0x0A, decode("A0 00 00 00 04 FA"), decode("03"), decode("A9 0F CD 55 AA 2D 5D 99 63 E3 5E D0 F4 40 17 76 99 83 2F 49 C6 BA B1 5C DA E5 79 4B E9 3F 93 4D 44 62 D5 D1 27 62 E4 8C 38 BA 83 D8 44 5D EA A7 41 95 A3 01 A1 02 B2 F1 14 EA DA 0D 18 0E E5 E7 A5 C7 3E 0C 4E 11 F6 7A 43 DD AB 5D 55 68 3B 14 74 CC 06 27 F4 4B 8D 30 88 A4 92 FF AA DA D4 F4 24 22 D0 E7 01 35 36 C3 C4 9A D3 D0 FA E9 64 59 B0 F6 B1 B6 05 65 38 A3 D6 D4 46 40 F9 44 67 B1 08 86 7D EC 40 FA AE CD 74 0C 00 E2 B7 A8 85 2D"));  
				pinpad.caImportKey(0x0B, decode("A0 00 00 00 04 FE"), decode("03"), decode("A6 53 EA C1 C0 F7 86 C8 72 4F 73 7F 17 29 97 D6 3D 1C 32 51 C4 44 02 04 9B 86 5B AE 87 7D 0F 39 8C BF BE 8A 60 35 E2 4A FA 08 6B EF DE 93 51 E5 4B 95 70 8E E6 72 F0 96 8B CD 50 DC E4 0F 78 33 22 B2 AB A0 4E F1 37 EF 18 AB F0 3C 7D BC 58 13 AE AE F3 AA 77 97 BA 15 DF 7D 5B A1 CB AF 7F D5 20 B5 A4 82 D8 D3 FE E1 05 07 78 71 11 3E 23 A4 9A F3 92 65 54 A7 0F E1 0E D7 28 CF 79 3B 62 A1"));
				pinpad.caImportKey(0x0C, decode("A0 00 00 00 04 FF"), decode("03"), decode("B8 55 CC 64 31 3A F9 9C 45 3D 18 16 42 EE 7D D2 1A 67 D0 FF 50 C6 1F E2 13 BC DC 18 AF BC D0 77 22 EF DD 25 94 EF DC 22 7D A3 DA 23 AD CC 90 E3 FA 90 74 53 AC C9 54 C4 73 23 BE DC F8 D4 86 2C 45 7D 25 F4 7B 16 D7 C3 50 2B E0 81 91 3E 5B 04 82 D8 38 48 40 65 DA 5F 66 59 E0 0A 9E 5D 57 0A DA 1E C6 AF 8C 57 96 00 75 11 95 81 FC 81 46 8D"));  
				pinpad.caImportKey(0x0D, decode("A0 00 00 00 03 01"), decode("03"), decode("C6 96 03 42 13 D7 D8 54 69 84 57 9D 1D 0F 0E A5 19 CF F8 DE FF C4 29 35 4C F3 A8 71 A6 F7 18 3F 12 28 DA 5C 74 70 C0 55 38 71 00 CB 93 5A 71 2C 4E 28 64 DF 5D 64 BA 93 FE 7E 63 E7 1F 25 B1 E5 F5 29 85 75 EB E1 C6 3A A6 17 70 69 17 91 1D C2 A7 5A C2 8B 25 1C 7E F4 0F 23 65 91 24 90 B9 39 BC A2 12 4A 30 A2 8F 54 40 2C 34 AE CA 33 1A B6 7E 1E 79 B2 85 DD 57 71 B5 D9 FF 79 EA 63 0B 75"));  
				pinpad.caImportKey(0x0E, decode("A0 00 00 00 03 03"), decode("03"), decode("B3 E5 E6 67 50 6C 47 CA AF B1 2A 26 33 81 93 50 84 66 97 DD 65 A7 96 E5 CE 77 C5 7C 62 6A 66 F7 0B B6 30 91 16 12 AD 28 32 90 9B 80 62 29 1B EC A4 6C D3 3B 66 A6 F9 C9 D4 8C ED 8B 4F C8 56 1C 8A 1D 8F B1 58 62 C9 EB 60 17 8D EA 2B E1 F8 22 36 FF CF F4 F3 84 3C 27 21 79 DC DD 38 4D 54 10 53 DA 6A 6A 0D 3C E4 8F DC 2D C4 E3 E0 EE E1 5F"));  
				pinpad.caImportKey(0x0F, decode("A0 00 00 00 03 94"), decode("01 00 01"), decode("D1 BE 39 61 5F 39 5A C9 33 7E 33 07 AA 5A 7A C3 5E AE 00 36 BF 20 B9 2F 9A 45 D1 90 B2 F4 61 6A BF 9D 34 0C BF 5F BB 3A 2B 94 BD 8F 2F 97 7C 0A 10 B9 0E 59 D4 20 1A A3 26 69 E8 CB E7 53 F5 36 11 9D F4 FB 5E 63 CE D8 7F 11 53 CE 91 4B 12 4F 3E 6B 64 8C D5 C9 76 55 F7 AB 4D F6 26 07 C9 5D A5 05 17 AB 8B E3 83 66 72 D1 C7 1B CD E9 BA 72 93 FF 34 82 F1 24 F8 66 91 13 0A B0 81 77 B0 2F 45 9C 02 5A 1F 3D FF E0 88 4C E7 81 22 54 2E A1 C8 EA 09 2B 55 2B 58 69 07 C8 3A D6 5E 0C 6F 91 A4 00 E4 85 E1 11 92 AA 4C 17 1C 5A 1E F5 63 81 F4 D0 91 CC 7E F6 BD 86 04 CB C4 C7 4D 5D 77 FF A0 7B 64 1D 53 99 8C DB 5C 21 B7 BC 65 E0 82 A6 51 3F 42 4A 4B 25 2E 0D 77 FA 40 56 98 6A 0A B0 CD A6 15 5E D9 A8 83 C6 9C C2 99 2D 49 EC BD 47 97 DD 28 64 FF C9 6B 8D"));  
				pinpad.caImportKey(0x10, decode("A0 00 00 00 03 95"), decode("03"), decode("BE 9E 1F A5 E9 A8 03 85 29 99 C4 AB 43 2D B2 86 00 DC D9 DA B7 6D FA AA 47 35 5A 0F E3 7B 15 08 AC 6B F3 88 60 D3 C6 C2 E5 B1 2A 3C AA F2 A7 00 5A 72 41 EB AA 77 71 11 2C 74 CF 9A 06 34 65 2F BC A0 E5 98 0C 54 A6 47 61 EA 10 1A 11 4E 0F 0B 55 72 AD D5 7D 01 0B 7C 9C 88 7E 10 4C A4 EE 12 72 DA 66 D9 97 B9 A9 0B 5A 6D 62 4A B6 C5 7E 73 C8 F9 19 00 0E B5 F6 84 89 8E F8 C3 DB EF B3 30 C6 26 60 BE D8 8E A7 8E 90 9A FF 05 F6 DA 62 7B"));  
				pinpad.caImportKey(0x11, decode("A0 00 00 00 03 96"), decode("03"), decode("B7 45 86 D1 9A 20 7B E6 62 7C 5B 0A AF BC 44 A2 EC F5 A2 94 2D 3A 26 CE 19 C4 FF AE EE 92 05 21 86 89 22 E8 93 E7 83 82 25 A3 94 7A 26 14 79 6F B2 C0 62 8C E8 C1 1E 38 25 A5 6D 3B 1B BA EF 78 3A 5C 6A 81 F3 6F 86 25 39 51 26 FA 98 3C 52 16 D3 16 6D 48 AC DE 8A 43 12 12 FF 76 3A 7F 79 D9 ED B7 FE D7 6B 48 5D E4 5B EB 82 9A 3D 47 30 84 8A 36 6D 33 24 C3 02 70 32 FF 8D 16 A1 E4 4D 8D"));  
				pinpad.caImportKey(0x12, decode("A0 00 00 00 03 97"), decode("03"), decode("AF 07 54 EA ED 97 70 43 AB 6F 41 D6 31 2A B1 E2 2A 68 09 17 5B EB 28 E7 0D 5F 99 B2 DF 18 CA E7 35 19 34 1B BB D3 27 D0 B8 BE 9D 4D 0E 15 F0 7D 36 EA 3E 3A 05 C8 92 F5 B1 9A 3E 9D 34 13 B0 D9 7E 7A D1 0A 5F 5D E8 E3 88 60 C0 AD 00 4B 1E 06 F4 04 0C 29 5A CB 45 7A 78 85 51 B6 12 7C 0B 29"));  
				pinpad.caImportKey(0x13, decode("A0 00 00 00 03 98"), decode("03"), decode("CA 02 6E 52 A6 95 E7 2B D3 0A F9 29 19 6E ED C9 FA F4 A6 19 F2 49 2E 3F B3 11 69 78 9C 27 6F FB B7 D4 31 16 64 7B A9 E0 D1 06 A3 54 2E 39 65 29 2C F7 78 23 DD 34 CA 8E EC 7D E3 67 E0 80 70 89 50 77 C7 EF AD 93 99 24 CB 18 70 67 DB F9 2C B1 E7 85 91 7B D3 8B AC E0 C1 94 CA 12 DF 0C E5 B7 A5 02 75 AC 61 BE 7C 3B 43 68 87 CA 98 C9 FD 39"));  
				pinpad.caImportKey(0x14, decode("A0 00 00 00 03 99"), decode("03"), decode("AB 79 FC C9 52 08 96 96 7E 77 6E 64 44 4E 5D CD D6 E1 36 11 87 4F 39 85 72 25 20 42 52 95 EE A4 BD 0C 27 81 DE 7F 31 CD 3D 04 1F 56 5F 74 73 06 EE D6 29 54 B1 7E DA BA 3A 6C 5B 85 A1 DE 1B EB 9A 34 14 1A F3 8F CF 82 79 C9 DE A0 D5 A6 71 0D 08 DB 41 24 F0 41 94 55 87 E2 03 59 BA B4 7B 75 75 AD 94 26 2D 4B 25 F2 64 AF 33 DE DC F2 8E 09 61 5E 93 7D E3 2E DC 03 C5 44 45 FE 7E 38 27 77"));  
				pinpad.caImportKey(0x15, decode("A0 00 00 00 03 90"), decode("03"), decode("C2 6B 3C B3 83 3E 42 D8 27 0D C1 0C 89 99 B2 DA 18 10 68 38 65 0D A0 DB F1 54 EF D5 11 00 AD 14 47 41 B2 A8 7D 68 81 F8 63 0E 33 48 DE A3 F7 80 38 E9 B2 1A 69 7E B2 A6 71 6D 32 CB F2 60 86 F1"));  
				pinpad.caImportKey(0x16, decode("A0 00 00 00 03 70"), decode("03"), decode("C7 DB 34 C5 14 C0 35 89 47 7E 41 DC CC 9D 05 40 53 9A 93 E9 D1 A8 A1 EE 41 07 3D 81 5B 1B 58 75 F9 77 F6 A1 36 D4 65 01 34 A1 1F 64 58 2E 3C 15 76 AB 74 36 96 E4 7A D9 04 8D C9 E3 68 BB 9C E1 CD BE 6D E5 D6 98 30 3B 48 2D CA EB D1 B8 7A D8 83 0F 86 1D 66 FC C5 62 9A 45 55 BE 35 AF C8 72 F9 EB 2B 0C 17 2F E7 AF EF 9B B3 FB 52 D7 F0 6E FD 74 62 AC 2E 54 2A 3C 95 FB 21 8C 01 AE 75 ED A1 3C 0C FF 29 49 1A A1 93 8F 7E 3B 85 E1 79 E0 CB 62 F3 90 8C 40 CE DB 99 C2 4A 28 38 52 1A 6B 97 44 01 89 EB 69 88 56 B8 99 71 94 8F E9 45 09 D7 C3 9C E6 FE 41 4E 20 61 27 20 FB 99 5B 5B 76 D3 82 4B 39 D3 4F 82 B4 D2 BE F0 A9 0C ED E6 4F 48 D8 68 EB B3 F0 E8 E4 49 A9 3D 33 22 A0 85 A0 42 1D FC C5 06 44 5F 4C 9A D4 9D B0 AA 02 3E F6 3C 66 2B 7E B0 A6 9C 2D"));  
				pinpad.caImportKey(0x17, decode("A0 00 00 00 03 80"), decode("01 00 01"), decode("80 2C 03 C2 07 E3 4D D1 A9 97 BA F3 7C 04 8D 15 9E 26 AF 37 C0 60 80 79 FA 30 20 EE B4 E9 AF F8 4D EB C4 A1 58 CA 16 C1 24 51 96 C8 91 3F 4E F4 98 68 D0 F4 DE 41 C3 E8 99 0F 6B 5E 9F 7C 8B E2 30 09 99 C1 6F 90 B0 2A 44 4D 9B 8A B0 56 7E F8 A6 E5 B9 68 CF 64 BD CE 8A D1 89 45 92 C6 C6 2E 21 DA 8B 3E C9 8B 50 F6 E6 AC 9B 56 BF 67 90 AC 7B 40 40 33 22 3D EC 14 05 64 23 07 F4 A7 67 D1"));  
				pinpad.caImportKey(0x18, decode("A0 00 00 00 03 82"), decode("01 00 01"), decode("BB 95 86 9C B2 D1 39 F6 7B 7C CF 55 93 B1 B5 7D 24 C3 4B B6 EF 0D F3 C2 02 AD 90 88 EA 11 D6 5B 90 F0 32 4B 46 0D 7F 3B 59 FF F7 BB 2C 63 AD C3 82 E0 59 87 BD B5 57 AB AB C7 D3 49 3D C2 78 5E DF 87 2A 95 7F F5 A2 12 D5 DA D8 B9 D2 A2 61 91 AE 64 F3 A3 DD 90 66 4B 4F D9 94 B0 AA BA 6F B0 E1 D4 4D 0D 9D 75 A0 7C 6A CA 37 D0 29 FF 44 B3 9B 69 FB 47 D9 E6 AE 26 AA C1 EC 7E 36 6D E3 A5 00 00 00 00 08 6A FB 6A 00 00 00 00 05 06 06 31"));  
				pinpad.caImportKey(0x19, decode("A0 00 00 00 03 83"), decode("03"), decode("AC 04 44 71 4C 14 F1 C9 CA EF 49 6E 9A 42 F1 0E 0B F8 2B C6 B5 93 6C 1B F9 A3 31 5A D0 1D 6A B0 01 FA D7 E5 C4 C8 68 04 6C 40 4A 81 C0 F2 DA 04 93 1C 23 4F FE 0C 66 D2 39 F5 8E 8B B7 C5 F8 1A 03 53 15 0F 73 9D 93 A8 1B 53 20 2B 6C 8C 85 E4 B7 95 78 54 88 24 EC 2E 1C D6 22 E4 66 20 51 68 3C 53 B8 F5 E5 1C F6 77 40 13 0A 46 31 23 D9 1E CB 17 B0 6B A7 C3 31 38 FD C3 DB 46 14 28 D0 98 C6 CE 97 4D DE 29 EF 86 EB 20 2B 5E 62 AE 09 52 C8 CD 23 B9 1E 0C 47 A8 AC B7 99 98 EC FD B3 91"));  
				pinpad.caImportKey(0x1A, decode("A0 00 00 00 03 84"), decode("01 00 01"), decode("AC 04 44 71 4C 14 F1 C9 CA EF 49 6E 9A 42 F1 0E 11 E7 60 4A E9 BB 76 91 C0 E4 53 A2 EF 21 16 7E DB CC FD 4E 86 58 6D B8 91 86 91 BA 29 2C 60 F2 4D CD EE 4A 0B 34 E3 FC 74 BD 27 9C 48 90 A0 E1 76 F9 3E 9D 49 B9 02 8D 2C DD F8 48 12 D6 9C 3E FE 10 58 9C CB 0B 98 3D F2 AA 92 65 12 8F A5 C4 17 ED 3F B8 D7 F6 69 D0 DD FF A6 A8 B8 D4 94 7E 8F EE 54 A5 59 5E 08 0F 9E 30 71 1A 7E 52 15 69 B5 EA B0 97 90 DF 21 F3 CF 98 F6 1B F0 CB 35 BC DB 21 3A 2C D9 81 CB 69 F6 74 6F 9C 93 F8 07 A7 F8 B8 65 47 EE 05 B3 83 4D 5D 30 3E D3 ED 83 6C 10 CC 00 66 A2 80 C5 6F 0E F4 9E 79 5E 41 A9 1B"));  
				pinpad.caImportKey(0x1B, decode("A0 00 00 00 05 01"), decode("03"), decode("D2 01 07 16 C9 FB 52 64 D8 C9 1A 14 F4 F3 2F 89 81 EE 95 4F 20 08 7E D7 7C DC 58 68 43 17 28 D3 63 7C 63 2C CF 27 18 A4 F5 D9 2E A8 AB 16 6A B9 92 D2 DE 24 E9 FB DC 7C AB 97 29 40 1E 91 C5 02 D7 2B 39 F6 86 6F 5C 09 8B 12 43 B1 32 AF EE 65 F5 03 6E 16 83 23 11 63 38 F8 04 08 34 B9 87 25"));  
				pinpad.caImportKey(0x1C, decode("A0 00 00 00 05 02"), decode("03"), decode("CF 42 64 E1 70 2D 34 CA 89 7D 1F 9B 66 C5 D6 36 91 EA CC 61 2C 8F 14 71 16 BB 22 D0 C4 63 49 5B D5 BA 70 FB 15 38 48 89 52 20 B8 AD EE C3 E7 BA B3 1E A2 2C 1D C9 97 2F A0 27 D5 42 65 BE BF 0A E3 A2 3A 8A 09 18 7F 21 C8 56 60 7B 98 BD A6 FC 90 81 16 81 6C 50 2B 3E 58 A1 45 25 4E EF EE 2A 33 35 11 02 24 02 8B 67 80 9D CB 80 58 E2 48 95"));  
				pinpad.caImportKey(0x1D, decode("A0 00 00 00 05 03"), decode("03"), decode("C2 49 07 47 FE 17 EB 05 84 C8 8D 47 B1 60 27 04 15 0A DC 88 C5 B9 98 BD 59 CE 04 3E DE BF 0F FE E3 09 3A C7 95 6A D3 B6 AD 45 54 C6 DE 19 A1 78 D6 DA 29 5B E1 5D 52 20 64 5E 3C 81 31 66 6F A4 BE 5B 84 FE 13 1E A4 4B 03 93 07 63 8B 9E 74 A8 C4 25 64 F8 92 A6 4D F1 CB 15 71 2B 73 6E 33 74 F1 BB B6 81 93 71 60 2D 89 70 E9 7B 90 07 93 C7 C2 A8 9A 4A 16 49 A5 9B E6 80 57 4D D0 B6 01 45"));				
				
				// Write CA keys to flash.		
				postLog("� Save CA keys to NVRAM\n");
				pinpad.caWriteKeysToFlash();
				
				postLog("\n");
			}
		});
	}
}
