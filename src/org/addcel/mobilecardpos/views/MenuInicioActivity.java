package org.addcel.mobilecardpos.views;

import org.addcel.mobilecardpos.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;

public class MenuInicioActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.menu_inicio_activity);

		Button primaPro = (Button) findViewById(R.id.btn_primaPro);
		Button bluePad = (Button) findViewById(R.id.btn_bluePad);

		primaPro.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuInicioActivity.this,
						PrimaProActivity.class));
			}
		});

		bluePad.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				startActivity(new Intent(MenuInicioActivity.this, PinpadPagoActivity.class));
			}
		});
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		//finish();
		//return;
		moveTaskToBack(true);
	}
}
