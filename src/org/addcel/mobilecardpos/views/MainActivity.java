package org.addcel.mobilecardpos.views;

import org.addcel.mobilecardpos.R;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.Window;

public class MainActivity extends Activity {
	ProgressDialog barProgressDialog;
	Handler updateBarHandler;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.splash_screen);
		Log.i("MobileCardPOS", "loadingSplash");

		updateBarHandler = new Handler();
		//launchRingDialog(getCurrentFocus());

	}

	private void checkNetwork() {
		ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInf = conMgr.getActiveNetworkInfo();
		if (networkInf != null && networkInf.isConnected()) {
			new Handler().postDelayed(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					startActivity(new Intent(MainActivity.this,
							MenuInicioActivity.class));
					finish();
				}
			}, 1000);		

		} else {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage("Sin conexi�n, �Desea conectartese a una red?")
					.setTitle("Advertencia")
					.setCancelable(false)
					.setNegativeButton("Cancelar",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int id) {
									// TODO Auto-generated method stub
									dialog.cancel();
									finish();
								}
							})
					.setPositiveButton("Continuar",
							new DialogInterface.OnClickListener() {

								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									// TODO Auto-generated method stub
									startActivity(new Intent(
											WifiManager.ACTION_PICK_WIFI_NETWORK));
								}
							});
			AlertDialog alert = builder.create();
			alert.show();
		}
	}

	public void launchRingDialog(View view) {
		final ProgressDialog ringProgressDialog = ProgressDialog.show(
				MainActivity.this, "Espere porfavor ...",
				"Cargando aplicaci�n...", true);
		new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {

					Thread.sleep(4000);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				ringProgressDialog.dismiss();
			}
		}).start();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		checkNetwork();
	}
}
