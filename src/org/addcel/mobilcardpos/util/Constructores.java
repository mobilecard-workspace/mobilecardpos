package org.addcel.mobilcardpos.util;

public class Constructores {

	//******************** TOKEN
	public static final String TOKEN = "token";
	
	//******************** PAGO
	public static final String ERROR_MSG = "mensajeError";
	public static final String AUTORIZACION = "autorizacion";
	
	public static final String CORREO = "email";
	public static final String NOMBRE = "nombre";
	public static final String TARJETA = "tarjeta";
	public static final String CVV2 = "cvv2";
	public static final String VIGENCIA = "vigencia";
	public static final String PRODUCTO = "descripcion";
	public static final String MONTO = "monto";
	public static final String IMEI = "imei";
	public static final String KEY = "key";
	//public static final String ID_TIPO_TARJETA = "idTipoTarjeta";
	public static final String LATITUD = "cy";
	public static final String LONGITUD = "cx";
	public static final String TIPO_DISP = "tipo";
	public static final String MODELO = "modelo";
	public static final String SOFTWARE = "software";
	
	public static final String TDCM = "tdcM";
	public static final String NUMERO_AUTORIZACION = "autorizacion";
	public static final String ADDCEL_IR = "addcelIR";
	public static final String ERROR_ID = "resultado";
	
	public static final String DESC_RECHAZO = "descRechazo";
	public static final String FOLIO = "folio";
	public static final String CARGO = "cargo";
	
	public static final String USER = "usuario";
	public static final String PASSWORD = "password";
	public static final String PREF_NAME = "Data";
	
	public static final String TIEMPO_NOTIFICACION = "tiempoNoti";
	public static final String TIEMPO_NOTIF_ANTERIOR = "tiempoNotiAnt";
	
	
}
