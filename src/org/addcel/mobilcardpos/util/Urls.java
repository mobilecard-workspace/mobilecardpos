package org.addcel.mobilcardpos.util;

public class Urls {
	private static final String desarrollo = "http://50.57.192.214:8080/VenezuelaDemoServicios";
	//private static final String produccion = "http://www.mobilecard.mx:8080/VenezuelaDemoServicios";

	private static final String context = desarrollo;

	public static final String url_getToken= context + "/getToken";
	public static final String url_pago = context + "/pago-visa_";	
}
