package org.addcel.interfaces;

import android.content.DialogInterface;

public interface DialogOkListener {
	public void ok(DialogInterface dialog, int id);

	public void cancel(DialogInterface dialog, int id);
}
