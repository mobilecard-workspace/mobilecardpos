package org.addcel.version;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.LinkedList;
import java.util.List;

import org.addcel.interfaces.WSResponseListener;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;
import android.util.Log;

public class VersionWSClient extends AsyncTask<VersionTO, Void, String> {

	private InputStream is;
	private String response;
	private String url;
	private WSResponseListener listener;

	private static final String TAG = "VERSION_CLIENT";

	public VersionWSClient(WSResponseListener listener, String url) {
		this.listener = listener;
		this.url = url;
	}

	@Override
	protected String doInBackground(VersionTO... params) {
		// TODO Auto-generated method stub
		try {
			DefaultHttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPost = new HttpPost(url);

			List<BasicNameValuePair> nameValuePairs = new LinkedList<BasicNameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("idPlataforma", params[0]
					.getIdPlataforma()));
			nameValuePairs.add(new BasicNameValuePair("idApp", params[0]
					.getIdApp()));
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

			HttpResponse httpResponse = httpClient.execute(httpPost);

			HttpEntity httpEntity = httpResponse.getEntity();
			is = httpEntity.getContent();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "UnsupportedEncodingException", e);
			return "";
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "ClientProtocolException", e);
			return "";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			Log.e(TAG, "IOException", e);
			return "";
		}

		try {

			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "UTF-8"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			response = sb.toString();
		} catch (Exception e) {
			Log.e(TAG, "Exception", e);
			return "";
		}

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		// TODO Auto-generated method stub
		listener.StringResponse(result);
	}

}
