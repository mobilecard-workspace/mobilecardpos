package org.addcel.util;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.util.Log;

public class AppUtils {

	private static String LOG = "AppUtils";

	public static String getVersionName(Context context, Class<?> cls) {
		try {
			ComponentName comp = new ComponentName(context, cls);
			PackageInfo pinfo = context.getPackageManager().getPackageInfo(
					comp.getPackageName(), 0);
			return pinfo.versionName;
		} catch (android.content.pm.PackageManager.NameNotFoundException e) {
			return null;
		}
	}

	public static LinkedList<String> getAniosVigencia() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);

		LinkedList<String> anios = new LinkedList<String>();

		int amount = anio + 7;

		for (int i = anio; i < amount; i++) {
			anios.add("" + i);
		}

		return anios;
	}

	public static List<String> getAniosConsulta() {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);

		List<String> anios = new LinkedList<String>();
		int amount = anio + 6;

		for (int i = anio; i < amount; i++) {
			anios.add("" + anio--);
		}

		return anios;
	}

	public static String getKey() {
		Long seed = new Date().getTime();

		String key = String.valueOf(seed).substring(0, 8);
		Log.d(LOG, "SENSITIVE KEY: " + key);

		return key;
	}

	public static int getAnioVigenciaPosition(String vigenciaAnio) {
		Calendar c = Calendar.getInstance();
		int anio = c.get(Calendar.YEAR);

		String anioInicial = String.valueOf(anio).substring(2, 4);
		int inicial = Integer.parseInt(anioInicial);
		int fin = Integer.parseInt(vigenciaAnio);

		return fin - inicial;
	}
}
