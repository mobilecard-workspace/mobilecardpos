package org.addcel.util;

import org.addcel.interfaces.DialogOkListener;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

public class Dialogos {
	private static ProgressDialog progress;

	public static ProgressDialog makeDialog(Context con, String title,
			String msj) {
		progress = new ProgressDialog(con);
		progress.setTitle(title);
		progress.setMessage(msj);
		progress.setIndeterminate(true);
		progress.show();
		return progress;
	}

	public static void closeDialog() {
		if (progress.isShowing()) {
			progress.dismiss();
		}
	}
	
	public static void makeYesNoAlert(Context con, String msj,
			final DialogOkListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj).setCancelable(false)
				.setPositiveButton("S�", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						listener.ok(dialog, id);
					}
				})
				.setNegativeButton("No", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
						dialog.cancel();
						listener.cancel(dialog, id);
					}
				});
		AlertDialog alert = builder.create();
		alert.show();
	}
	
	public static void makeOkAlert(Context con, String msj,
			final DialogOkListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(con);
		builder.setMessage(msj)
				.setCancelable(false)
				.setPositiveButton("Aceptar",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								listener.ok(dialog, id);
							}
						});

		AlertDialog alert = builder.create();

		alert.show();
	}
	/*
	 * 
	 * public static void makeYesNoAlert(Context con, String msj, final
	 * DialogOkListener listener){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(con); builder.setMessage(msj) .setCancelable(false)
	 * .setPositiveButton("S�", new DialogInterface.OnClickListener() { public
	 * void onClick(DialogInterface dialog, int id) { listener.ok(dialog, id); }
	 * }) .setNegativeButton("No", new DialogInterface.OnClickListener() {
	 * public void onClick(DialogInterface dialog, int id) { dialog.cancel();
	 * listener.cancel(dialog, id); } }); AlertDialog alert = builder.create();
	 * alert.show(); }
	 * 
	 * public static void makeAcceptAlert(Context con, String msj, final
	 * DialogOkListener listener){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(con); builder.setMessage(msj) .setCancelable(false)
	 * .setPositiveButton("Acepto", new DialogInterface.OnClickListener() {
	 * public void onClick(DialogInterface dialog, int id) { listener.ok(dialog,
	 * id); } }) .setNegativeButton("No acepto", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int id) { dialog.cancel(); listener.cancel(dialog, id); } });
	 * AlertDialog alert = builder.create(); alert.show(); }
	 * 
	 * public static void makeReenvioAlert(Context con, String msj, final
	 * DialogOkListener listener){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(con); builder.setMessage(msj) .setCancelable(false)
	 * .setPositiveButton("Reenviar", new DialogInterface.OnClickListener() {
	 * public void onClick(DialogInterface dialog, int id) { listener.ok(dialog,
	 * id); } }) .setNegativeButton("Reenviar a otro correo", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int id) { dialog.cancel(); listener.cancel(dialog, id); } });
	 * AlertDialog alert = builder.create(); alert.show(); }
	 * 
	 * public static void showInitialDialog(final Activity ctx) {
	 * AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
	 * alertDiaBuilder
	 * .setTitle(ctx.getResources().getText(R.string.dialog_title))
	 * .setMessage(ctx.getResources().getText(R.string.dialog_message))
	 * .setCancelable(false) .setPositiveButton(R.string.dialog_ok_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub dialog.cancel(); } });
	 * 
	 * AlertDialog alertDialog = alertDiaBuilder.create(); alertDialog.show(); }
	 * 
	 * public static void makeResultadosAlert(Context con, String msj, final
	 * DialogOkListener listener){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(con); builder.setMessage(msj) .setCancelable(false)
	 * .setPositiveButton("Actualizar datos", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int id) { listener.ok(dialog, id); } })
	 * .setNegativeButton("Modificar password", new
	 * DialogInterface.OnClickListener() { public void onClick(DialogInterface
	 * dialog, int id) { dialog.cancel(); listener.cancel(dialog, id); } });
	 * AlertDialog alert = builder.create(); alert.show(); }
	 * 
	 * public static void makeOkAlert(Context con, String msj, final
	 * DialogOkListener listener){ AlertDialog.Builder builder = new
	 * AlertDialog.Builder(con); builder.setMessage(msj) .setCancelable(false)
	 * .setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
	 * public void onClick(DialogInterface dialog, int id) { listener.ok(dialog,
	 * id); } });
	 * 
	 * AlertDialog alert = builder.create();
	 * 
	 * alert.show(); }
	 * 
	 * public static void showPlacaDialog(final SherlockActivity ctx, final
	 * PlacaDataSource data, final int servicio) {
	 * 
	 * EditText placaEdit = new EditText(ctx); placaEdit.setLayoutParams(new
	 * LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	 * placaEdit.setInputType(InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS);
	 * 
	 * final EditText finalPlaca = placaEdit;
	 * 
	 * AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
	 * alertDiaBuilder
	 * .setTitle(ctx.getResources().getString(R.string.placa_dialog_title))
	 * .setMessage(ctx.getResources().getString(R.string.placa_dialog_message))
	 * .setView(placaEdit)
	 * .setNegativeButton(R.string.placa_dialog_cancel_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub dialog.cancel(); } })
	 * .setPositiveButton(R.string.placa_dialog_add_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub final String placa =
	 * finalPlaca.getText().toString().trim().toUpperCase();
	 * 
	 * if (null == placa || "".equals(placa)) { Toast.makeText(ctx,
	 * "Introduzca una placa", Toast.LENGTH_SHORT).show(); } else {
	 * Log.i("showPlacaDialog", "abrimos dataSource"); data.open();
	 * 
	 * if (data.insertaNuevo(placa) > 0) { Log.i("showPlacaDialog",
	 * "Exito en inserci�n"); Toast.makeText(ctx,
	 * "Su placa se ha insertado con �xito.", Toast.LENGTH_LONG).show(); }
	 * else { Toast.makeText(ctx,
	 * "Error en inserci�n de placa. Intente de nuevo m�s tarde.",
	 * Toast.LENGTH_SHORT).show(); } Log.i("showPlacaDialog",
	 * "cerramos dataSource"); data.close(); Log.i("showPlacaDialog",
	 * "despedimos dialog"); dialog.dismiss();
	 * 
	 * if (Servicios.TENENCIA == servicio) { ctx.startActivity(new Intent(ctx,
	 * TenenciaActivity.class)); } else if (Servicios.INFRACCION == servicio) {
	 * ctx.startActivity(new Intent(ctx, InfraccionActivity.class)); }
	 * ctx.finish(); } } });
	 * 
	 * AlertDialog alertDialog = alertDiaBuilder.create(); alertDialog.show(); }
	 * 
	 * public static void showPredialDialog(final int tramite, final
	 * SherlockActivity ctx, final PredialDataSource data) {
	 * 
	 * EditText placaEdit = new EditText(ctx); placaEdit.setLayoutParams(new
	 * LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	 * placaEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
	 * 
	 * final EditText finalPlaca = placaEdit;
	 * 
	 * AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
	 * alertDiaBuilder
	 * .setTitle(ctx.getResources().getString(R.string.predial_dialog_title))
	 * .setMessage
	 * (ctx.getResources().getString(R.string.predial_dialog_message))
	 * .setView(placaEdit)
	 * .setNegativeButton(R.string.placa_dialog_cancel_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub dialog.cancel(); } })
	 * .setPositiveButton(R.string.placa_dialog_add_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub final String cuenta =
	 * finalPlaca.getText().toString().trim().toUpperCase();
	 * 
	 * if (null == cuenta || "".equals(cuenta)) { Toast.makeText(ctx,
	 * "Introduzca una cuenta de predial", Toast.LENGTH_SHORT).show(); } else {
	 * Log.i("showPlacaDialog", "abrimos dataSource"); data.open();
	 * 
	 * if (data.insertaNuevo(cuenta) > 0) { Log.i("showPlacaDialog",
	 * "Exito en inserci�n"); Toast.makeText(ctx,
	 * "Su cuenta predial se ha insertado con �xito.",
	 * Toast.LENGTH_LONG).show();
	 * 
	 * } else { Toast.makeText(ctx,
	 * "Error en inserci�n de cuenta predial. Intente de nuevo m�s tarde.",
	 * Toast.LENGTH_SHORT).show(); } Log.i("showPlacaDialog",
	 * "cerramos dataSource"); data.close(); Log.i("showPlacaDialog",
	 * "despedimos dialog"); dialog.dismiss();
	 * 
	 * if (Tramite.PREDIAL == tramite) { ctx.startActivity(new Intent(ctx,
	 * PredialActivity.class)); } else if (Tramite.PREDIAL_VENCIDO == tramite) {
	 * ctx.startActivity(new Intent(ctx, PredialVencidoActivity.class)); }
	 * 
	 * ctx.finish(); } } });
	 * 
	 * AlertDialog alertDialog = alertDiaBuilder.create(); alertDialog.show(); }
	 * 
	 * public static void showEmailDialog(final SherlockActivity ctx, final
	 * JSONObject data, final WebServiceClient client) {
	 * 
	 * 
	 * EditText placaEdit = new EditText(ctx); placaEdit.setLayoutParams(new
	 * LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
	 * placaEdit.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
	 * 
	 * final EditText finalPlaca = placaEdit;
	 * 
	 * AlertDialog.Builder alertDiaBuilder = new AlertDialog.Builder(ctx);
	 * alertDiaBuilder
	 * .setTitle(ctx.getResources().getString(R.string.mail_dialog_title))
	 * .setMessage(ctx.getResources().getString(R.string.mail_dialog_message))
	 * .setView(placaEdit)
	 * .setNegativeButton(R.string.placa_dialog_cancel_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) { // TODO
	 * Auto-generated method stub dialog.cancel(); } })
	 * .setPositiveButton(R.string.placa_dialog_send_button, new
	 * DialogInterface.OnClickListener() {
	 * 
	 * public void onClick(DialogInterface dialog, int which) {
	 * dialog.dismiss(); // TODO Auto-generated method stub final String correo
	 * = finalPlaca.getText().toString().trim();
	 * 
	 * if (null == correo || "".equals(correo)) { Toast.makeText(ctx,
	 * "Introduzca un correo electr�nico", Toast.LENGTH_SHORT).show(); } else
	 * { Log.i("Dialogos", data.toString()); String request =
	 * Request.buildReenvioRequest(data.optString("idUsuario"),
	 * data.optString("idBitacora"), data.optInt("idProducto"), correo);
	 * client.execute(request); } } });
	 * 
	 * AlertDialog alertDialog = alertDiaBuilder.create(); alertDialog.show(); }
	 * 
	 * public static void BuildCenteredToast(Context _con, String msg) { Toast t
	 * = new Toast(_con); t.setGravity(Gravity.CENTER, 0, 0); t.setText(msg);
	 * t.setDuration(Toast.LENGTH_SHORT); t.show(); }
	 */
}
