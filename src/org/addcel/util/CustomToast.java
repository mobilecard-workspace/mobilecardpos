package org.addcel.util;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class CustomToast {

	private static Toast toast;

	public static void build(Context _con, String _text) {
		toast = Toast.makeText(_con, _text, Toast.LENGTH_SHORT);
		toast.setGravity(Gravity.CENTER, 0, 0);
		toast.show();
	}
}
